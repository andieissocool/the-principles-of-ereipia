﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockBridge : MonoBehaviour
{
    public GameObject hints;
    public GameObject text;
    public GameObject KeyTwo;
    public GameObject Value;
    public GameObject left;
    public GameObject right;
    public GameObject bogen;
    public GameObject saveObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("z")) {
            KeyTwo.SetActive(true);
            StartCoroutine(OpenDoors());
        }
    }

    void OnTriggerEnter() {
        Text texti = text.GetComponent<Text>();
        texti.text = "Press 'z' to unlock the bridge";
        //saveObject.GetComponent<SaveGame>().Save();
        hints.SetActive(true);
        StartCoroutine(Waiting(3f));
    }

    IEnumerator Waiting(float wait)
    {
        yield return new WaitForSeconds(wait);
        hints.SetActive(false);
    }

    IEnumerator OpenDoors() {
        yield return new WaitForSeconds(3);
        Value.SetActive(false);
        KeyTwo.SetActive(false);
        left.SetActive(false);
        right.SetActive(false);
        bogen.SetActive(false);
    }
}
