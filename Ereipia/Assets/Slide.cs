﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GAD;
using KinematicCharacterController;

public class Slide : MonoBehaviour
{
    public GameObject hints;
    public GameObject text;

    private GameObject m_Player;
    bool sliding;
    public GameObject placeA;
    public GameObject placeB;
    Vector3 speedTest = new Vector3(1.0f, 1.0f, 1.0f);
    public float speed = 1.0f;
    bool destination;

    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        m_Player = GameObject.FindWithTag("Player");
        sliding = false;
        Vector3 oldPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!sliding) {
            
        }

        if (Input.GetKeyDown("m")) {
            sliding = true;
            player.GetComponent<KinematicCharacterMotor>().SetPosition(placeB.transform.position);
            //m_Player.transform.position = placeB.transform.position;
            //StartCoroutine(Wait(1.0f));
        }

        /*if (sliding) {
            float step =  speed * Time.deltaTime;
            
            if (player.transform.position != placeB.transform.position) {
                player.GetComponent<KinematicCharacterMotor>().SetPosition(placeA.transform.position + placeB.transform.position - speedTest);
            }
            else {
                sliding = false;
            }
            //player.GetComponent<KinematicCharacterMotor>().SetPosition(Vector3.MoveTowards(placeA.transform.position, placeB.transform.position, speed));
            //m_Player.transform.position = Vector3.MoveTowards(placeA.transform.position, placeB.transform.position, speed);
            //m_Player.transform.position = Vector3.MoveTowards(placeA.transform.position, placeB.transform.position, speed);

            //if (placeA.transform.position == placeB.transform.position) {
                //sliding = false;
            //}
        }*/

    }

    void OnTriggerEnter() {
        Text texti = text.GetComponent<Text>();
        texti.text = "Press 'm' to slide to the city";
        hints.SetActive(true);
        StartCoroutine(Waiting(3f));
    }

    IEnumerator Wait(float wait) {
        yield return new WaitForSeconds(wait);
    }

    IEnumerator Waiting(float wait)
    {
        yield return new WaitForSeconds(wait);
        hints.SetActive(false);
    }

}
