﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class keyEnter : MonoBehaviour
{
    public GameObject hints;
    public GameObject text;
    public GameObject particles;
    public GameObject key;
    public GameObject collideBridge;

    public GameManager gameManager;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("y")) {
            Destroy(particles);
            Destroy(key);
            //particles.SetActive(false);
            //key.SetActive(false);
            collideBridge.SetActive(true);
            Text texti = text.GetComponent<Text>();
            texti.text = "Now you can open the bridge!";
            hints.SetActive(true);
            StartCoroutine(Waiting(3f));
            gameManager.GetComponent<GameManager>().puzzleCounter += 1;
            gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
        }
    }

    void OnTriggerEnter() {
        Text texti = text.GetComponent<Text>();
        texti.text = "You found the key for the bridge! Press 'y'";
        hints.SetActive(true);
        StartCoroutine(Waiting(3f));
    }

    IEnumerator Waiting(float wait)
    {
        yield return new WaitForSeconds(wait);
        hints.SetActive(false);
    }
}
