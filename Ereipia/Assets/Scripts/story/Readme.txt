StoryProgression

Prinzipiell wird alles via Collider und Trigger gemacht.
Alle einzelnen StoryStates besitzen genau einen Abschnitt der jeweils eingeschalten wird und die anderen werden hierbei dann ausgeschalten.

Ich hab einen leere Gruppe mit dem Namen StoryElements gemacht. Darunter dann f�r den jeweiligen Part nen StoryPart1.... das kann man prinzipiell so fortsetzen, dann muss nur am Ende die Gruppe ausgeschalten werden.
Die Untersuchungstrigger bleiben jedoch. Dh. wenn etwas zum einscannen ist zum freischalten eines Eintrages im Men� dann wird ein Bool auf True gesetzt, dass es das nicht mehr anzeigt beim Raycast. Weiteres weiter unten beim Abschnitt Trigger.


Audioelemente - audioTrigger
prinzipiell recht einfach:
- collider aufziehen f�r die Stelle wo das Audio getriggert werden soll
- diesen auf is trigger stellen
- audioSource drauf legen
- play on awaken ausschalten
- Script audioTrigger unter story folder drauf ziehen
- Untertitel reinkopieren
- GameManager verlinken

Audio wird genau einmal abgespielt und der Untertitel ist 8s eingeblendet, wenn der Spieler das erste Mal drauf geht


Untersuchung von Objekten
- dem Objekt, das Untersucht werden soll erst einmal einen Box Collider geben (falls noch nicht vorhanden)
- dann den Tag interactableObj geben (ist der letzte in der Liste, die anderen lassen sich leider nimma l�schen)
- das Script interactableTarget draufziehen
- hier gibt es dann verschiedenen Optionen
- outline script draufziehen (Quick Outline - scripts - Outline)

- wenn das Objekt nur betrachtet werden soll und nur ein Untertitel erzeugt werden soll braucht kein Bool angekreuzt werden --> hier nur den subtitle �bertragen, der wird dann getoggelt wenn der raycast draufgeht

- kann man das Objekt Untersuchen und es ist verkn�pft mit dem GUI:
  - GUI Anzeigetext vergeben und anhackeln
  - R is prinzipiell jetzt mal der Untersuchungskey
  - Description String is die Beschreibung (2 kurze Worte max)

- Log ist f�r das aktivieren eines Logs im Men�
- Open GUI ist daf�r wenn das GUI nach dem Untersuchen gleich ge�ffnet werden soll 
  - Object Type ist f�r den Type des Objektes was aktiviert werden soll, hier gibt es folgendes: 
    generalPicture, general, mcCarter, avory

- Anzeigetext + Audio
- ist wenn beim Untersuchen nur ein Audio abgespielt werden soll
- collider �ber den Bereich ziehen oder einem Objekt einen collider geben
- outline script raufziehen
- subtitle vergeben, show interaction text anhackeln und beschreibung eingeben
- has audio anhackeln und sound to play und volumne angeben
- collider / objekt ein audioSource geben

Weiters gibt es einen trigger f�r nur LogFileObjekte
- funktioniert �hnlich
- collider erstellen
- auf is trigger stellen
- guiTrigger script draufziehen
- Type: persLog, logFile
- fertig

