﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guiTrigger : MonoBehaviour
{
    public GameManager gameManager;
    public string fileType;
    public logFileObj logFile;
    public persLogObj persLog;

    public bool alreadyTriggered;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (fileType == "logFile" && !alreadyTriggered)
        {
            logFile.active = true;
            gameManager.newLog.SetActive(true);
            alreadyTriggered = true;
        } else if (fileType == "persLog" && !alreadyTriggered)
        {
            persLog.active = true;
            gameManager.newLog.SetActive(true);
            alreadyTriggered = true;    
        }
    }
}
