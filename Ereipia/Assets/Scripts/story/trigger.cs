﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class trigger : MonoBehaviour
{
    public GameManager gameManager;
    public string subtitle;

    private void OnTriggerEnter(Collider other)
    {
        gameManager.subtitleText.text = subtitle;
        gameManager.subtibtleMenu.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        gameManager.subtibtleMenu.SetActive(false);
    }
}
