﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class audioTrigger : MonoBehaviour
{
    public GameManager gameManager;

    public string subtitle;

    public AudioClip SoundToPlay;
    public float Volume;
    AudioSource audio;
    public bool alreadyPlayed = false;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void OnTriggerEnter()
    {
        if (!alreadyPlayed)
        {
            StartCoroutine(wait());
            audio.PlayOneShot(SoundToPlay, Volume);
            alreadyPlayed = true;
        }
    }

    IEnumerator wait()
    {
        gameManager.subtibtleMenu.SetActive(true);
        gameManager.subtitleText.text = subtitle;

        yield return new WaitForSeconds(8);

        gameManager.subtibtleMenu.SetActive(false);
    }
}
