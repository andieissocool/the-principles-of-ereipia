﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class logFileTrigger : MonoBehaviour
{
    public GameManager gameManager;
    public List<logFileObj> logFile = new List<logFileObj>();
    

    public bool alreadyTriggered;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        foreach (logFileObj log in logFile)
        {
            log.active = true;
            gameManager.newRepairedLog.SetActive(true);
            log.decryphed = true;
        }
    }
}
