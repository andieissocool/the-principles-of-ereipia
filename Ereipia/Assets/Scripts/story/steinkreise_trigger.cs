﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class steinkreise_trigger : MonoBehaviour
{

    public GameManager gameManager;
    private void OnTriggerEnter()
    {
        gameManager.steinkreis_active = true;
        gameManager.steinkreisGUI.SetActive(true);
        gameManager.steinkreisVolume.SetActive(true);
    }

    private void OnTriggerExit()
    {
        gameManager.steinkreis_active = false;
        gameManager.steinkreisGUI.SetActive(false);
        gameManager.steinkreisVolume.SetActive(false);

    }
}
