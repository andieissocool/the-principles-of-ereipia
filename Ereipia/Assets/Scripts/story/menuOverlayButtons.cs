﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuOverlayButtons : MonoBehaviour
{
    public GameManager gameManager;

    public void clickRepairs()
    {
        gameManager.repairObj.SetActive(true);
        gameManager.repairText.text = "5%";
        gameManager.warningMenu.SetActive(false);
        gameManager.firstRepairStart = true;
    }

    public void clickCancel()
    {
        gameManager.warningMenu.SetActive(false);
    }
}
