﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class interactableTarget : MonoBehaviour
{

    [Header("state Machine")]
    public GameManager gameManager;

    [Header("GUI Anzeigetext")]
    public bool showInteractionText;
    public string keyString;
    public string descriptionString;

    [Header("Subtitle")]
    public bool subtitlyOnly;
    public string subtitle;

    [Header("Audio")]
    public bool hasAudio;
    public AudioClip SoundToPlay;
    public float Volume;

    [Header("Log")]
    public bool activateLog;
    public string objectType;
    public logFileObj logFileObj;

    [Header("Open Gui")]
    public bool openGui;
    public GameObject activeGui;
}
