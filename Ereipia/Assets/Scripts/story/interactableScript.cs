﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class interactableScript: MonoBehaviour
{
    [SerializeField]
    private string interactableTag = "Selectable";    // tag for interactable objects

    // keep track of the current selection
    private Transform currentSelection;

    [Header("GameManager")]
    public GameManager gameManager;

    AudioSource audio;

    // Update is called once per frame
    void Update()
    {

        if (gameManager.SM.CurrentState == gameManager.walkingState)
        {
            if(currentSelection != null)
            {
                currentSelection.GetComponent<Outline>().enabled = false;
                currentSelection = null;
                gameManager.interactionMenu.SetActive(false);
                gameManager.subtibtleMenu.SetActive(false);
            }

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit target;

            if (Physics.Raycast(ray, out target))
            {

                var selection = target.transform;

                if (selection.CompareTag(interactableTag))
                {
            
                    gameManager.GUI.SetActive(true);
                    if(selection.GetComponent<interactableTarget>().subtitlyOnly == true)
                    {
                        gameManager.subtibtleMenu.SetActive(true);
                        gameManager.subtitleText.text = selection.GetComponent<interactableTarget>().subtitle;
                        selection.GetComponent<Outline>().enabled = true;
                    }

                    if (selection.GetComponent<interactableTarget>().showInteractionText == true && selection.GetComponent<interactableTarget>().hasAudio == false)
                    {
                        gameManager.keyLetter.text = selection.GetComponent<interactableTarget>().keyString;
                        gameManager.keyDescription.text = selection.GetComponent<interactableTarget>().descriptionString;
                        gameManager.interactionMenu.SetActive(true);

                        gameManager.subtibtleMenu.SetActive(true);
                        gameManager.subtitleText.text = selection.GetComponent<interactableTarget>().subtitle;
                        selection.GetComponent<Outline>().enabled = true;

                        if (selection.GetComponent<interactableTarget>().openGui == true)
                        {
                            if (Input.GetKeyDown("r"))
                            {
                                if (selection.GetComponent<interactableTarget>().objectType == "generalPicture")
                                {
                                    selection.GetComponent<interactableTarget>().logFileObj.active = true;
                                    selection.GetComponent<interactableTarget>().openGui = false;
                                    selection.GetComponent<interactableTarget>().showInteractionText = false;

                                    logFileObj logFileObj = selection.GetComponent<interactableTarget>().logFileObj;

                                    gameManager.GUI.SetActive(false);
                                    gameManager.interactionMenu.SetActive(false);
                                    gameManager.subtibtleMenu.SetActive(false);

                                    selection.GetComponent<Outline>().enabled = false;
                                    selection = null;

                                    gameManager.SM.ChangeState(gameManager.peaMenu);
                                    gameManager.peaMainMenu.SetActive(false);
                                    gameManager.msgMenu.SetActive(true);

                                    gameManager.generalButton.GetComponent<Button>().Select();

                                    gameManager.generalMenu.SetActive(true);
                                    gameManager.mcMenu.SetActive(false);
                                    gameManager.avoryMenu.SetActive(false);

                                    gameManager.generalTextCont.SetActive(false);
                                    gameManager.generalWithPicTextCont.SetActive(true);

                                    gameManager.generalPicHL.text = logFileObj.headline;
                                    gameManager.generalPic.sprite = logFileObj.img;
                                    gameManager.generalPicText.text = logFileObj.description;

                                    logFileObj = null;
                                }

                                else if (selection.GetComponent<interactableTarget>().objectType == "generalPicture")
                                {
                                    selection.GetComponent<interactableTarget>().logFileObj.active = true;
                                    selection.GetComponent<interactableTarget>().openGui = false;
                                    selection.GetComponent<interactableTarget>().showInteractionText = false;

                                    logFileObj logFileObj = selection.GetComponent<interactableTarget>().logFileObj;

                                    gameManager.GUI.SetActive(false);
                                    gameManager.interactionMenu.SetActive(false);
                                    gameManager.subtibtleMenu.SetActive(false);

                                    selection.GetComponent<Outline>().enabled = false;
                                    selection = null;

                                    gameManager.SM.ChangeState(gameManager.peaMenu);
                                    gameManager.peaMainMenu.SetActive(false);
                                    gameManager.msgMenu.SetActive(true);

                                    gameManager.generalButton.GetComponent<Button>().Select();

                                    gameManager.generalMenu.SetActive(true);
                                    gameManager.mcMenu.SetActive(false);
                                    gameManager.avoryMenu.SetActive(false);

                                    gameManager.generalTextCont.SetActive(true);
                                    gameManager.generalWithPicTextCont.SetActive(false);

                                    gameManager.generalHL.text = logFileObj.headline;
                                    gameManager.generalText.text = logFileObj.description;

                                    logFileObj = null;
                                }

                                else if(selection.GetComponent<interactableTarget>().objectType == "mcCarter")
                                {
                                    selection.GetComponent<interactableTarget>().logFileObj.active = true;
                                    selection.GetComponent<interactableTarget>().openGui = false;
                                    selection.GetComponent<interactableTarget>().showInteractionText = false;

                                    logFileObj logFileObj = selection.GetComponent<interactableTarget>().logFileObj;

                                    gameManager.GUI.SetActive(false);
                                    gameManager.interactionMenu.SetActive(false);
                                    gameManager.subtibtleMenu.SetActive(false);

                                    selection.GetComponent<Outline>().enabled = false;
                                    selection = null;

                                    gameManager.SM.ChangeState(gameManager.peaMenu);
                                    gameManager.peaMainMenu.SetActive(false);
                                    gameManager.msgMenu.SetActive(true);

                                    gameManager.generalButton.GetComponent<Button>().Select();

                                    gameManager.generalMenu.SetActive(false);
                                    gameManager.mcMenu.SetActive(true);
                                    gameManager.avoryMenu.SetActive(false);

                                    gameManager.generalTextCont.SetActive(true);
                                    gameManager.generalWithPicTextCont.SetActive(false);

                                    gameManager.generalHL.text = logFileObj.headline;
                                    gameManager.generalText.text = logFileObj.description;

                                    logFileObj = null;
                                }

                                else if(selection.GetComponent<interactableTarget>().objectType == "avory")
                                {
                                    selection.GetComponent<interactableTarget>().logFileObj.active = true;
                                    selection.GetComponent<interactableTarget>().openGui = false;
                                    selection.GetComponent<interactableTarget>().showInteractionText = false;

                                    logFileObj logFileObj = selection.GetComponent<interactableTarget>().logFileObj;

                                    gameManager.GUI.SetActive(false);
                                    gameManager.interactionMenu.SetActive(false);
                                    gameManager.subtibtleMenu.SetActive(false);

                                    selection.GetComponent<Outline>().enabled = false;
                                    selection = null;

                                    gameManager.SM.ChangeState(gameManager.peaMenu);
                                    gameManager.peaMainMenu.SetActive(false);
                                    gameManager.msgMenu.SetActive(true);

                                    gameManager.generalButton.GetComponent<Button>().Select();

                                    gameManager.generalMenu.SetActive(false);
                                    gameManager.mcMenu.SetActive(false);
                                    gameManager.avoryMenu.SetActive(true);

                                    gameManager.generalTextCont.SetActive(true);
                                    gameManager.generalWithPicTextCont.SetActive(false);

                                    gameManager.generalHL.text = logFileObj.headline;
                                    gameManager.generalText.text = logFileObj.description;

                                    logFileObj = null;
                                }
                            }
                        }
                    }
                    else if (selection.GetComponent<interactableTarget>().showInteractionText == true && selection.GetComponent<interactableTarget>().hasAudio == true)
                    {
                        gameManager.keyLetter.text = selection.GetComponent<interactableTarget>().keyString;
                        gameManager.keyDescription.text = selection.GetComponent<interactableTarget>().descriptionString;
                        gameManager.interactionMenu.SetActive(true);

                        audio = selection.GetComponent<AudioSource>();

                        if (Input.GetKeyDown("r"))
                        {
                            gameManager.subtibtleMenu.SetActive(true);
                            string text = selection.GetComponent<interactableTarget>().subtitle;
                            StartCoroutine(wait(text));
                            audio.PlayOneShot(selection.GetComponent<interactableTarget>().SoundToPlay, selection.GetComponent<interactableTarget>().Volume);
                        }
                    }
                    


                    
                    currentSelection = selection;
                }
            }


        }


        IEnumerator wait(string text)
        {
            gameManager.subtitleText.text = text;

            yield return new WaitForSeconds(8);

            gameManager.subtibtleMenu.SetActive(false);
        }
    }
}
