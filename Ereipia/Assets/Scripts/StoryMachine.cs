﻿using System.Collections;
using UnityEngine;

public class StoryMachine : MonoBehaviour
{
    public StoryState CurrentState;

    public void Initialize(StoryState state)
    {
        CurrentState = state;
        state.EnterState();
    }

    public void ChangeState(StoryState newStoryState)
    {
        CurrentState.ExitState();

        CurrentState = newStoryState;
        newStoryState.EnterState();
    }
}
