﻿using System.Collections;

public abstract class State
{
    protected StateMachine stateMachine;
    protected GameManager gameManager;

    protected State(StateMachine stateMachine, GameManager gameManager)
    {
        this.stateMachine = stateMachine;
        this.gameManager = gameManager;
    }

    public virtual void EnterState()
    {
 
    }

    public virtual void ExitState()
    {
        
    }

    public virtual void Update()
    {
       
    }

}
