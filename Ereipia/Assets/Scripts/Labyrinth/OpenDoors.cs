﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoors : MonoBehaviour
{
    private GameObject m_Player;
    public GameObject middlePart;
    public GameObject opener;

    public float speed = 0.5f;
     private void Start()
     {
         m_Player = GameObject.FindWithTag("Character");
     }
 
     private void Update()
     {
         if (m_Player.GetComponent<Labyrinth>().TriggerDefine == 1)
         {
            if (middlePart.transform.rotation.eulerAngles.y < 123)
            {
                middlePart.transform.Rotate(Vector3.up * speed * (Time.deltaTime / 6));
            }
         }

         if (m_Player.GetComponent<Labyrinth>().TriggerDefine == 2)
         {
            Destroy(opener);
         }

     }
}
