﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Labyrinth : MonoBehaviour
{
    public int TriggerDefine;
 
     public void OnTriggerEnter(Collider other)
     {
         if (other.tag == "first")
         {
             TriggerDefine = 1;
         }

         if (other.tag == "second")
         {
             TriggerDefine = 2;
         }

         if (other.tag == "three")
         {
             TriggerDefine = 3;
         }
     }
}
