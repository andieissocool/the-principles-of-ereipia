﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bees : MonoBehaviour
{
    private int current;

    public float speed;
    
    public Transform[] target;

    private GameObject m_Player;
    public GameObject topTre;
    private Vector3 vec;

    //maze breakdown
    public GameObject flash;
    public GameObject plane;
    public GameObject circle1;
    public GameObject value;
    public GameObject particle;

    // Start is called before the first frame update
    void Start()
    {
        m_Player = GameObject.FindWithTag("Character");
        vec = topTre.transform.position + new Vector3(3.0f, 0f, 0f);

        //nur zum testen!!!
        //m_Player.GetComponent<Labyrinth>().TriggerDefine = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Player.GetComponent<Labyrinth>().TriggerDefine == 3)
         {
            topTre.transform.position = Vector3.MoveTowards(topTre.transform.position, vec, 0.5f * Time.deltaTime);
            //StartCoroutine(Playing());

            if (topTre.transform.position == vec) {
                // make bees move
                if (transform.position != target[current].position)
                {
                    Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime);
                    GetComponent<Rigidbody>().MovePosition(pos);
                }
                else if (transform.position == target[2].position)
                {
                    GetComponent<Rigidbody>().transform.position = target[2].position;
                    flash.SetActive(true);
                }

                else {
                    current = (current + 1) % target.Length;
                }

                // break away maze
                Destroy(plane, 1);
                Destroy(circle1, 1);
                value.SetActive(true);
                particle.SetActive(true);
            }
         }
    }

    //IEnumerator Playing() {
        //topTre.GetComponent<AudioSource>().Play();
        //yield return new WaitForSeconds(0.1f);
    //}
}
