﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Maze : MonoBehaviour
{
    // click before one: show interface
    // click one: middle dreht sich, left verschwindet, Interface taucht auf
    // click two:  unten und grenze verschwindet, Interface taucht auf

    public GameObject secretMaze;
    public GameObject secretSigns;
    
    //public GameObject oneCollider;
    //public GameObject two;

    private GameObject m_Player;
    public GameObject middlePart;
    public GameObject opener1;
    public GameObject above;
    public GameObject below;
    public GameObject scanVolume;

    public float speed = 5.0f;

     private void Start()
     {
         m_Player = GameObject.FindWithTag("Character");
     }

    // Update is called once per frame
    void Update()
    {
        

        // make hints appear
        if (Input.GetKeyDown("u")) {
            scanVolume.SetActive(true);
            secretSigns.SetActive(false);
            secretMaze.SetActive(true);
            StartCoroutine(Waiting(3f));
        }
        
        if (m_Player.GetComponent<Labyrinth>().TriggerDefine == 1)
         {
            if (middlePart.transform.rotation.eulerAngles.y < 95)
            {
                middlePart.transform.Rotate(Vector3.up * speed * Time.deltaTime * 10);
                Destroy(opener1);
            }
         }

         if (m_Player.GetComponent<Labyrinth>().TriggerDefine == 2)
         {
            Destroy(above);
            Destroy(below);
         }
    }

    IEnumerator Waiting(float wait)
    {
        yield return new WaitForSeconds(wait);
        secretSigns.SetActive(true);
        secretMaze.SetActive(false);
        scanVolume.SetActive(false);
    }
}
