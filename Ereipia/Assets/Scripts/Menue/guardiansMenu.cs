﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class guardiansMenu : MonoBehaviour
{
    [Header("Guardian Buttons Container")]
    public GameObject[] guardianButtonsCont;

    [Header("Guardian Buttons")]
    public GameObject[] guardianButtons;

    [Header("Guardian Objects")]
    public guardiansObject[] guardians;

    [Header("Arrow Objects")]
    public GameObject arrowLeftCont;
    public GameObject arrowRightCont;

    private int buttonsActive = 0;
    private int active;

    [Header("Container for Infos")]
    public Text headline;
    public Text description;
    public Image runeImg;
    public Image guardImg;
    public GameObject audioButton;

    private void Start()
    {
        for(int i = 0; i < guardians.Length; i++)
        {
            if (guardians[i].active)
            {
                guardianButtonsCont[i].SetActive(true);
                buttonsActive = buttonsActive + 1;
            }
        }

        if(buttonsActive > 1)
        {
            arrowLeftCont.SetActive(true);
            arrowRightCont.SetActive(true);
        }

        guardianButtons[0].GetComponent<Button>().Select();

        headline.text = guardians[0].headline;
        description.text = guardians[0].descriptioin;
        runeImg.sprite = guardians[0].rune;
        guardImg.sprite = guardians[0].guardImg;
        audioButton.GetComponent<ButtonAudio>().setClickFX(guardians[0].frequency);
    }

    

    
    public void guard1Active()
    {
        headline.text = guardians[0].headline;
        description.text = guardians[0].descriptioin;
        runeImg.sprite = guardians[0].rune;
        guardImg.sprite= guardians[0].guardImg;
        audioButton.GetComponent<ButtonAudio>().setClickFX(guardians[0].frequency);
        active = 0;
    }

    public void guard2Active()
    {
        headline.text = guardians[1].headline;
        description.text = guardians[1].descriptioin;
        runeImg.sprite = guardians[1].rune;
        guardImg.sprite = guardians[1].guardImg;
        audioButton.GetComponent<ButtonAudio>().setClickFX(guardians[1].frequency);
        active = 1;
    }
    public void guard3Active()
    {
        headline.text = guardians[2].headline;
        description.text = guardians[2].descriptioin;
        runeImg.sprite = guardians[2].rune;
        guardImg.sprite = guardians[2].guardImg;
        audioButton.GetComponent<ButtonAudio>().setClickFX(guardians[2].frequency);
        active = 2;
    }

    public void guard4Active()
    {
        headline.text = guardians[3].headline;
        description.text = guardians[3].descriptioin;
        runeImg.sprite = guardians[3].rune;
        guardImg.sprite = guardians[3].guardImg;
        audioButton.GetComponent<ButtonAudio>().setClickFX(guardians[3].frequency);
        active = 3;
    }
    

    public void arrowLeft()
    {
        if (active > 0 && active < buttonsActive)
        {
            active = active - 1;
        } else if (active == 0)
        {
            active = buttonsActive - 1;
        }

        setActive(active);
    }

    public void arrowRight()
    {
        if(active >= 0 && active < buttonsActive - 1)
        {
            active = active + 1;
        } else if(active == buttonsActive - 1)
        {
            active = 0;
        }

        setActive(active);
    }

    private void setActive(int active)
    {
        guardianButtons[active].GetComponent<Button>().Select();

        if (active == 0)
        {
            guard1Active();
        }
        else if (active == 1)
        {
            guard2Active();
        }
        else if (active == 2)
        {
            guard3Active();
        }
        else if (active == 3)
        {
            guard4Active();
        }
    }
}
