﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class journalScrollListButton : MonoBehaviour
{
    [Header("Buttons serialized")]
    [SerializeField]
    public Text menu;

    [Header("GameObjects without Picture")]
    public Text headlineCont;
    public Text descriptionCont;

    [Header("logFileObj Array")]
    public persLogObj[] logs;

    private int index;

    /*activation bool*/
    public bool activated;

    public void Start()
    {
        activeButton(0);
    }

    public void SetMenuText(string menuText)
    {
        menu.text = menuText;
    }

    public void SetButtonIndex(int i)
    {
        index = i;
    }

    public void onClick()
    {
        activeButton(index);
    }

    public void activeButton(int index)
    {
        headlineCont.text = logs[index].headline;

        if (logs[index].update1Exists && logs[index].update1Active && logs[index].update2Exists == false)
        {
            descriptionCont.text = logs[index].description + "\n\n" + logs[index].update1;

        } else if (logs[index].update2Exists && logs[index].update2Exists && logs[index].update2Active)
        {
            descriptionCont.text = logs[index].description + "\n\n" + logs[index].update1 + "\n\n" + logs[index].update2;
        } else
        {
            descriptionCont.text = logs[index].description;
        }
        


    }
}
