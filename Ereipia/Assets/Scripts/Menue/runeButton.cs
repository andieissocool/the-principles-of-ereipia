﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class runeButton : MonoBehaviour
{
    [Header("Object with RunesMenuScript")]
    public GameObject runesMenu;

    private int index;

    // Start is called before the first frame update
    public void OnClick()
    {
        Debug.Log(index);

        runesMenu.GetComponent<runesMenu>().letter.text = runesMenu.GetComponent<runesMenu>().runesObj[index].letter;
        runesMenu.GetComponent<runesMenu>().meaning.text = runesMenu.GetComponent<runesMenu>().runesObj[index].meaning;
        runesMenu.GetComponent<runesMenu>().description.text = runesMenu.GetComponent<runesMenu>().runesObj[index].description;

        runesMenu.GetComponent<runesMenu>().runeImg.sprite = runesMenu.GetComponent<runesMenu>().runesObj[index].runeImg;

        runesMenu.GetComponent<runesMenu>().setRunesActive(index);

    }

    public void audioOnClick()
    {
        runesMenu.GetComponent<runesMenu>().letter.text = runesMenu.GetComponent<runesMenu>().audioRunesObj[index].letter;
        runesMenu.GetComponent<runesMenu>().meaning.text = runesMenu.GetComponent<runesMenu>().audioRunesObj[index].meaning;
        runesMenu.GetComponent<runesMenu>().description.text = runesMenu.GetComponent<runesMenu>().audioRunesObj[index].description;

        runesMenu.GetComponent<runesMenu>().runeImg.sprite = runesMenu.GetComponent<runesMenu>().audioRunesObj[index].runeImg;

        if (runesMenu.GetComponent<runesMenu>().audioRunesObj[index].audioExists)
        {
            var audioButton = runesMenu.GetComponent<runesMenu>().audioButton.GetComponent<ButtonAudio>();
            audioButton.setClickFX(runesMenu.GetComponent<runesMenu>().audioRunesObj[index].audio);
        }

        runesMenu.GetComponent<runesMenu>().setAudioRuneActive(index);
    }

    public void setButtonIndex(int i)
    {
        index = i;
    }
}
