﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "persLog", menuName = "persLog")]
public class persLogObj : ScriptableObject
{
    [Header("Activate")]
    public bool active;

    [Header("Base Info")]
    public string menue;
    public string headline;
    [TextArea]
    public string description;

    [Header("Update #1")]
    public bool update1Exists;
    public bool update1Active;
    [TextArea]
    public string update1;
    

    [Header("Update #2")]
    public bool update2Exists;
    public bool update2Active;
    [TextArea]
    public string update2;
}
