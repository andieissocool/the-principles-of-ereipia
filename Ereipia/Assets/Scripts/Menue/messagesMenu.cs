﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class messagesMenu : MonoBehaviour
{
    [Header("Active Menus")]
    public bool generalActive;
    public bool avoryActive;
    public bool mcCarterActive;

    [Header("Menue Buttons")]
    public GameObject generalButton;
    public GameObject avoryButton;
    public GameObject mcCarterButton;

    [Header("Menue Scrollbar Container")]
    public GameObject generalScrollbar;
    public GameObject avoryScrollbar;
    public GameObject mcCarterScrollbar;

    [Header("Menue Scrollist Content")]
    public GameObject generalContent;
    public GameObject avoryContent;
    public GameObject mcCarterContent;

    [Header("First Object")]
    public logFileObj general1st;
    public logFileObj avory1st;
    public logFileObj mcCarter1st;

    [Header("Layout for Picture and without")]
    public GameObject withoutPic;
    public GameObject withPic;

    [Header("GameObjects to set text")]
    public Text headlineCont;
    public Text descriptionCont;
    public Image imgCont;

    public void Start()
    {
        withoutPic.SetActive(true);
        withPic.SetActive(false);

        if (generalActive)
        {
            generalButton.SetActive(true);
            generalContent.transform.GetChild(0).GetComponent<Button>().Select();

            headlineCont.text = general1st.headline;
            descriptionCont.text = general1st.description;
        } 
        if (avoryActive)
        {
            avoryButton.SetActive(true);
        }
        if(mcCarterActive)
        {
            mcCarterButton.SetActive(true);
        }
        
        avoryScrollbar.SetActive(false);
        mcCarterScrollbar.SetActive(false);
        generalScrollbar.SetActive(true);

        generalButton.GetComponent<Button>().Select();
    }

    public void generalMenu()
    {
        generalButton.GetComponent<Button>().Select();
        generalContent.transform.GetChild(0).GetComponent<Button>().Select();

        avoryScrollbar.SetActive(false);
        mcCarterScrollbar.SetActive(false);
        generalScrollbar.SetActive(true);

        withoutPic.SetActive(true);
        withPic.SetActive(false);

        headlineCont.text = general1st.headline;
        descriptionCont.text = general1st.description;

        if(general1st.imgExists == true)
        {
            imgCont.sprite = general1st.img;
        }
    }

    public void avoryMenu()
    {
        avoryButton.GetComponent<Button>().Select();
        avoryContent.transform.GetChild(1).GetComponent<Button>().Select();

        generalScrollbar.SetActive(false);
        mcCarterScrollbar.SetActive(false);
        avoryScrollbar.SetActive(true);

        withoutPic.SetActive(true);
        withPic.SetActive(false);

        headlineCont.text = avory1st.headline;
        descriptionCont.text = avory1st.description;
    }

    public void mcCarterMenu()
    {
        mcCarterButton.GetComponent<Button>().Select();
        mcCarterContent.transform.GetChild(0).GetComponent<Button>().Select();

        generalScrollbar.SetActive(false);
        avoryScrollbar.SetActive(false);
        mcCarterScrollbar.SetActive(true);

        withoutPic.SetActive(true);
        withPic.SetActive(false);

        headlineCont.text = mcCarter1st.headline;
        descriptionCont.text = mcCarter1st.description;
    }
}
