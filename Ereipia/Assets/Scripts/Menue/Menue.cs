﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menue : MonoBehaviour
{
    public GameManager gameManager;

    public void PlayGame()
    {
        if (PlayerPrefs.GetString("ActiveScene") == "Landmass")
        {
            SceneManager.LoadScene("Landmass");
        }
        else
        {
            SceneManager.LoadScene("AudioPuzzle");
        }

    }

    public void NewGame()
    {
        PlayerPrefs.SetInt("Counter", 0);
        PlayerPrefs.SetFloat("CharachterX", (float)1280.4);
        PlayerPrefs.SetFloat("CharachterY", (float)262.9);
        PlayerPrefs.SetFloat("CharachterZ", (float)433.6);

        PlayerPrefs.SetInt("loadAudioPuzzle", 0);
        PlayerPrefs.SetInt("counterAudioPuzzle", 0);
        PlayerPrefs.SetString("resultAudioPuzzle", "");
        PlayerPrefs.SetInt("AudioPuzzleCrystalAktivated", 0);

        PlayerPrefs.SetString("StoryState", gameManager.begining.ToString());
        PlayerPrefs.SetString("State", gameManager.walkingState.ToString());
        SceneManager.LoadScene("Landmass");
    }

    public void QuitGame()
    {
        Debug.Log("quit");
        gameManager.saveObject.GetComponent<SaveGame>().Save();
        Application.Quit();
    }

    public void Save()
    {
        gameManager.saveObject.GetComponent<SaveGame>().Save();
        Debug.Log("GAME SAVED");
    }

    public void MainMenue()
    {
        SceneManager.LoadScene("MainMenue");
    }

    public void continueGame()
    {
        gameManager.peaMainMenu.SetActive(false);
        gameManager.msgMenu.SetActive(false);
        gameManager.runesMenu.SetActive(false);
        gameManager.guardiansMenu.SetActive(false);
        gameManager.logMenu.SetActive(false);
        gameManager.GUI.SetActive(false);

        gameManager.fpsControllerGroup.SetActive(true);

        if (SceneManager.GetActiveScene().name == "AudioPuzzle")
        {
            gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
        }
        else
        {
            gameManager.SM.ChangeState(gameManager.walkingState);
        }
    }
}
