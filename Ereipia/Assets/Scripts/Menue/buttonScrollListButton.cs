﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonScrollListButton : MonoBehaviour
{
    [Header("Buttons serialized")]
    [SerializeField]
    public Text menu;

    [Header("Layout for Picture and without")]
    public GameObject withoutPic;
    public GameObject withPic;

    [Header("GameObjects without Picture")]
    public Text headlineCont;
    public Text descriptionCont;

    [Header("GameObject with Picture")]
    public Text headlinePicCont;
    public Text descriptionPicCont;
    public Image pictureCont;

    [Header("logFileObj Array")]
    public logFileObj[] messages;

    private int index;

    /*activation bool*/
    public bool activated;

    public void Start()
    {
        activeButton(0);
    }

    public void SetMenuText(string menuText)
    {
        menu.text = menuText;
    }
    
    public void SetButtonIndex(int i)
    {
        index = i;
    }

    public void onClick()
    {
        activeButton(index);
    }

    public void activeButton(int index)
    {
        if (messages[index].imgExists)
        {
            withoutPic.SetActive(false);
            withPic.SetActive(true);

            headlinePicCont.text = messages[index].headline;
            descriptionPicCont.text = messages[index].description;
            pictureCont.sprite = messages[index].img;

        } else
        {
            withPic.SetActive(false);
            withoutPic.SetActive(true);

            if (messages[index].logFile)
            {
                if (messages[index].decryphed)
                {
                    headlineCont.text = "Logfile " + messages[index].logfile + ": " + messages[index].headline + " [" + messages[index].day + "]";
                    descriptionCont.text = messages[index].description;
                }
                else
                {
                    headlineCont.text = "Logfile " + messages[index].logfile + ": " + messages[index].headlinCorr + " [" + messages[index].dayCorr + "]";
                    descriptionCont.text = messages[index].descriptionCorr;
                }
            } else
            {
                headlineCont.text = messages[index].headline;
                descriptionCont.text = messages[index].description;
            }

            
        }
    }
}
