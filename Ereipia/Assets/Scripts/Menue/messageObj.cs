﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "message", menuName = "message")]
public class messageObj : ScriptableObject
{
    [Header("Activation")]
    public bool active;

    [Header("Content")]
    public string menuString;
    public string headline;
    [TextArea]
    public string description;

    [Header("Media")]
    public bool imgExists;
    public Sprite img;
    public bool audioExists;
    public AudioClip audio;

    public string category;
    public int activateOn;
}
