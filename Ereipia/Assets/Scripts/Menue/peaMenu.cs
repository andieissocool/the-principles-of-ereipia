﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class peaMenu : MonoBehaviour
{
    public GameManager gameManager;

    [Header("Menus")]
    public GameObject peaStartMenu;
    public GameObject journalMenu;
    public GameObject messagesMenu;
    public GameObject guardiansMenu;
    public GameObject runesMenu;

    [Header("Menu Buttons")]
    public GameObject journalButton;
    public GameObject messagesButton;
    public GameObject decodingButton;
    public GameObject scanSurroundingButton;
    public GameObject analyzeFrequencyButton;
    public GameObject EncryptingEnzyclopediaButton;

    private GameObject currentMenu, nextMenu;

    private void Start()
    {
        Debug.Log("PEA Menu started");
        currentMenu = peaStartMenu;
    }

    public void peaStartMenuActive()
    {
        currentMenu.SetActive(false);
        peaStartMenu.SetActive(true);
        currentMenu = peaStartMenu;

        journalButton.GetComponent<Animator>().SetTrigger("Normal");
        messagesButton.GetComponent<Animator>().SetTrigger("Normal");
        scanSurroundingButton.GetComponent<Animator>().SetTrigger("Normal");
        analyzeFrequencyButton.GetComponent<Animator>().SetTrigger("Normal");
        EncryptingEnzyclopediaButton.GetComponent<Animator>().SetTrigger("Normal");
    }

    public void journalMenuActive()
    {
        currentMenu.SetActive(false);
        journalMenu.SetActive(true);
        currentMenu = journalMenu;
    }

    public void messagesMenuActive()
    {
        currentMenu.SetActive(false);
        messagesMenu.SetActive(true);
        currentMenu = messagesMenu;
    }

    public void guardiansMenuActive()
    {
        currentMenu.SetActive(false);
        guardiansMenu.SetActive(true);
        currentMenu = guardiansMenu;
    }

    public void runesMenuActive()
    {
        currentMenu.SetActive(false);
        runesMenu.SetActive(true);
        currentMenu = runesMenu;
    }

    public void enterScanMode()
    {
        gameManager.peaMainMenu.SetActive(false);
        gameManager.msgMenu.SetActive(false);
        gameManager.runesMenu.SetActive(false);
        gameManager.guardiansMenu.SetActive(false);
        gameManager.logMenu.SetActive(false);
        gameManager.GUI.SetActive(false);

        gameManager.SM.ChangeState(gameManager.deviceScan);
    }
}
