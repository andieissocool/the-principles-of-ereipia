﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "rune", menuName = "rune")]
public class runeObject : ScriptableObject
{
    [Header("activation")]
    public bool active;

    [Header("Content")]
    public string letter;
    public string meaning;
    [TextArea]
    public string description;

    [Header("media")]
    public Sprite runeImg;
    public Sprite ScanImg;

    [Header("audio")]
    public bool audioExists;
    public AudioClip audio;
}
