﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonScrollListControl : MonoBehaviour
{
    [Header("Serialized Button Templates")]
    [SerializeField]
    private GameObject buttonTemplate;

    [Header("Top Menu Buttons")]
    public GameObject button;

    [Header("logFile Objects for each f the Top Menus")]
    public logFileObj[] messages;

    [Header("Content Button List to clear before changing Menus")]
    public GameObject buttonListContent;

    private int index = 0;

    private void Start()
    {

        logFileObjEntries(messages, buttonTemplate, "start");
    }

    public void buttonOnClick()
    {
        logFileObjEntries(messages, buttonTemplate, "next");
    }

    public void logFileObjEntries (logFileObj[] log, GameObject buttonTemplate, string list)
    {
        int forI = 0;

        if (list == "next")
        {
            forI = index + 1;
        }

        for (int i = forI; i < log.Length; i++)
        {
            if (log[i].active == true)
            {
                GameObject button = Instantiate(buttonTemplate) as GameObject;
                button.SetActive(true);

                if(log[i].decryphed == false)
                {
                    button.GetComponent<buttonScrollListButton>().SetMenuText(log[i].menuCorr);
                } else
                {
                    button.GetComponent<buttonScrollListButton>().SetMenuText(log[i].menu);
                }

                button.GetComponent<buttonScrollListButton>().SetButtonIndex(i);

                //button parent will be set to the conten object -- world position false
                button.transform.SetParent(buttonTemplate.transform.parent, false);

                index = i;
            }
        }
    }
}
