﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class runesMenu : MonoBehaviour
{
    [Header("Menu buttons")]
    public GameObject runesMB;
    public GameObject audioRunesMB;

    [Header("Container RunesAuswahl")]
    public GameObject runesSlider;
    public GameObject audioRunesSlider;

    [Header("Runes")]
    public runeObject[] runesObj;
    public GameObject[] runesButtonsCont;
    public GameObject[] runesButton;
    public GameObject runesArrowLeft;
    public GameObject runesArrowRight;

    [Header("Audio Runes")]
    public runeObject[] audioRunesObj;
    public GameObject[] audioRunesButtonsCont;
    public GameObject[] audioRunesButton; 
    public GameObject audioRunesArrowLeft;
    public GameObject audioRunesArrowRight;

    private int runeButtonsActive = 0;
    private int audioRuneButtonsActive = 0;
    private int runesActive;
    private int audioRunesActive;

    [Header("Container for Infos")]
    public Text letter;
    public Text meaning;
    public Text description;
    public Image runeImg;
    public GameObject audioButton;

    public void Start()
    {
        audioRunesSlider.SetActive(false);
        audioButton.SetActive(false);
        runesSlider.SetActive(true);
        runesMB.GetComponent<Button>().Select();

        for (int i = 0; i < runesObj.Length; i++)
        {
            if (runesObj[i].active)
            {
                runesButtonsCont[i].SetActive(true);
                runeButtonsActive = runeButtonsActive + 1;
            }

            if (runeButtonsActive > 1)
            {
                runesArrowLeft.SetActive(true);
                runesArrowRight.SetActive(true);
            }

            runesButton[0].GetComponent<Button>().Select();
            runesButton[i].GetComponent<runeButton>().setButtonIndex(i);

            letter.text = runesObj[0].letter;
            meaning.text = runesObj[0].meaning;
            description.text = runesObj[0].description;
            runeImg.sprite = runesObj[0].runeImg;
        }
    }

    public void onClickRunes()
    {
        audioRunesSlider.SetActive(false);
        audioButton.SetActive(false);
        runesSlider.SetActive(true);
        runesMB.GetComponent<Button>().Select();

        for (int i = 0; i < runesObj.Length; i++)
        {
            if (runesObj[i].active)
            {
                runesButtonsCont[i].SetActive(true);
                runeButtonsActive = runeButtonsActive + 1;
            }

            if (runeButtonsActive > 1)
            {
                runesArrowLeft.SetActive(true);
                runesArrowRight.SetActive(true);
            }

            runesButton[0].GetComponent<Button>().Select();
            runesButton[i].GetComponent<runeButton>().setButtonIndex(i);

            letter.text = runesObj[0].letter;
            meaning.text = runesObj[0].meaning;
            description.text = runesObj[0].description;
            runeImg.sprite = runesObj[0].runeImg;
        }
    }

    public void onClickAudoRunes()
    {
        runesSlider.SetActive(false);
        audioButton.SetActive(true);
        audioRunesSlider.SetActive(true);
        audioRunesMB.GetComponent<Button>().Select();



        for (int i = 0; i < audioRunesObj.Length; i++)
        {
            if (audioRunesObj[i].active)
            {
                audioRunesButtonsCont[i].SetActive(true);
                audioRuneButtonsActive = audioRuneButtonsActive + 1;
            }

            if (audioRuneButtonsActive > 1)
            {
                audioRunesArrowLeft.SetActive(true);
                audioRunesArrowRight.SetActive(true);
            }

            audioRunesButton[0].GetComponent<Button>().Select();
            audioRunesButton[i].GetComponent<runeButton>().setButtonIndex(i);

            letter.text = audioRunesObj[0].letter;
            meaning.text = audioRunesObj[0].meaning;
            description.text = audioRunesObj[0].description;
            runeImg.sprite = audioRunesObj[0].runeImg;

            if (audioRunesObj[0].audioExists)
            {
               audioButton.GetComponent<ButtonAudio>().setClickFX(audioRunesObj[0].audio);
            }
        }
    }

    public void setRunesActive(int i)
    {
        runesActive = i;
    }

    public void setAudioRuneActive(int i)
    {
        audioRunesActive = i;
    }

    public void runesArrowLeftClick()
    {
        if (runesActive > 0 && runesActive < runeButtonsActive)
        {
            runesActive = runesActive - 1;
        }
        else if (runesActive == 0)
        {
            runesActive = runeButtonsActive - 1;
        }

        setRuneElement(runesActive);
        Debug.Log(runesActive);
    }
    public void runesArrowRightClick()
    {
        if (runesActive >= 0 && runesActive < runeButtonsActive - 1)
        {
            runesActive = runesActive + 1;
        }
        else if (runesActive == runeButtonsActive - 1)
        {
            runesActive = 0;
        }

        setRuneElement(runesActive);
    }
    public void audioRunesArrowLeftClick()
    {
        if (audioRunesActive > 0 && audioRunesActive < audioRuneButtonsActive)
        {
            audioRunesActive = audioRunesActive - 1;
        }
        else if (audioRunesActive == 0)
        {
            audioRunesActive = audioRuneButtonsActive - 1;
        }

        setAudioRuneElement(audioRunesActive);
    }
    public void audioRunesArrowRightClick()
    {
        if (audioRunesActive >= 0 && audioRunesActive < audioRuneButtonsActive - 1)
        {
            audioRunesActive = audioRunesActive + 1;
        }
        else if (audioRunesActive == audioRuneButtonsActive - 1)
        {
            audioRunesActive = 0;
        }

        setAudioRuneElement(audioRunesActive);
    }

    private void setRuneElement(int i)
    {
        runesButton[i].GetComponent<Button>().Select();
        letter.text = runesObj[i].letter;
        meaning.text = runesObj[i].meaning;
        description.text = runesObj[i].description;
        runeImg.sprite = runesObj[i].runeImg;
    }

    private void setAudioRuneElement (int i)
    {
        audioRunesButton[i].GetComponent<Button>().Select();
        letter.text = audioRunesObj[i].letter;
        meaning.text = audioRunesObj[i].meaning;
        description.text = audioRunesObj[i].description;
        runeImg.sprite = audioRunesObj[i].runeImg;

        audioButton.GetComponent<ButtonAudio>().setClickFX(audioRunesObj[i].audio);
    }

    public void audioOnClick(int i)
    {
        audioButton.GetComponent<ButtonAudio>().setClickFX(audioRunesObj[i].audio);
        audioRunesButton[audioRunesActive].GetComponent<Button>().Select();
    }

}
