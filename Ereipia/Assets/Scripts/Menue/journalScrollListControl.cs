﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class journalScrollListControl : MonoBehaviour
{

    [Header("Serialized Button Templates")]
    [SerializeField]
    private GameObject buttonTemplate;

    [Header("logFile Objects for each f the Top Menus")]
    public persLogObj[] logs;

    private int index = 0;

    private void Start()
    {
        logFileObjEntries(logs, buttonTemplate, "start");
    }

    public void buttonOnClick()
    {
        logFileObjEntries(logs, buttonTemplate, "next");
    }

    public void logFileObjEntries(persLogObj[] log, GameObject buttonTemplate, string list)
    {
        int forI = 0;

        if (list == "next")
        {
            forI = index + 1;
        }

        for (int i = forI; i < log.Length; i++)
        {
            if (log[i].active == true)
            {
                GameObject button = Instantiate(buttonTemplate) as GameObject;
                button.SetActive(true);

                button.GetComponent<journalScrollListButton>().SetMenuText(log[i].menue);
                button.GetComponent<journalScrollListButton>().SetButtonIndex(i);

                //button parent will be set to the conten object -- world position false
                button.transform.SetParent(buttonTemplate.transform.parent, false);

                index = i;
            }
        }
    }
}
