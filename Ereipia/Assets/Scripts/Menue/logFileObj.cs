﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "logFile", menuName = "logFile")]
public class logFileObj : ScriptableObject
{
    [Header("Activation")]
    public bool active;

    [Header("Day only for Researcher Log Files")]
    public int logfile;

    [Header("File Data")]
    public bool decryphed;
    public string menu;
    public string headline;
    public string day;
    [TextArea]
    public string description;

    [Header("Corrupted File Data")]
    public string menuCorr;
    public string headlinCorr;
    public string dayCorr;
    [TextArea]
    public string descriptionCorr;
    
    [Header("Media Data")]
    public bool imgExists;
    public Sprite img;
    public bool audioExists;
    public AudioClip audio;
    public AudioClip audioCorr;

    [Header("Check if corrupted Data exists")]
    public bool logFile;            // logFile when needed decyphering

    [Header("Category / Activation Time")]
    public string category;
    public int activateOn;
}
