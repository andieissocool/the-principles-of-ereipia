﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "memoryCard", menuName = "memoryCard")]
public class memoryCardObj : ScriptableObject
{
    [Header("Activation")]
    public bool active;

    [Header("Contet")]
    public string menue;
    public string headline;
    [TextArea]
    public string description;

    [Header("Update 1")]
    [TextArea]
    public string update1; 
    public bool update1Exists;
    public bool update1Active;

    [Header("Update 2")]
    [TextArea]
    public string update2;
    public bool update2exists;
    public bool update2Active;
}
