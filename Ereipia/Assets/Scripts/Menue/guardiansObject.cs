﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newGuardian", menuName = "guardian")]
public class guardiansObject : ScriptableObject
{
    [Header("Activation")]
    public bool active;

    [Header("Content")]
    public string headline;
    [TextArea]
    public string descriptioin;

    [Header("Media")]
    public Sprite rune;
    public Sprite guardImg;
    public Sprite runeScanImg;
    public AudioClip frequency;
}