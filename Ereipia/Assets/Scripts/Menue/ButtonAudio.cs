﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAudio : MonoBehaviour
{
    [Header("AudioSource Element")]
    public AudioSource myFx;

    [Header("Event Sounds")]
    public AudioClip hoverFx;
    public AudioClip clickFX;
    
    public void HoverSound()
    {
        myFx.PlayOneShot(hoverFx);
    }
    public void ClickSound()
    {
        myFx.PlayOneShot(clickFX);
    }

    public void setClickFX(AudioClip sound)
    {
        clickFX = sound;
    }
}
