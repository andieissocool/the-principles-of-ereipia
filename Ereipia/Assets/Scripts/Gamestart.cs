﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gamestart : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("Start Screen loaded");
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(30, 30, 150, 30), "Start Game"))
        {
            SceneManager.LoadScene("Landmass");
        }
    }
}
