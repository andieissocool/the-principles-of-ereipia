﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Beginning : StoryState
{
	public Beginning(StoryMachine storyMachine, GameManager gameManager) : base(storyMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("beginning");
		gameManager.storyState1.SetActive(true);
	}

	public override void ExitState()
	{
		gameManager.storyState1.SetActive(false);
		gameManager.storySTate2.SetActive(true);
	}

	public override void Update()
	{
		if (Input.GetKeyDown("r"))
		{
			Debug.Log("rrrrrrr");
			gameManager.StoryM.ChangeState(gameManager.city);
		}
	}

}