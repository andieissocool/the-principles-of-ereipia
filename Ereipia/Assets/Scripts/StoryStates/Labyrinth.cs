﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LabyrinthStory : StoryState
{
	public LabyrinthStory(StoryMachine storyMachine, GameManager gameManager) : base(storyMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("LabyrinthStory Start");
	}

	public override void ExitState()
	{
		Debug.Log("LabyrinthStory End");
	}

	public override void Update()
	{
	
	}

}