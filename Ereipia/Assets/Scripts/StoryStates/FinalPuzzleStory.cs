﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalPuzzleStory: StoryState
{
	public FinalPuzzleStory(StoryMachine storyMachine, GameManager gameManager) : base(storyMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("FinalPuzzleStory Start");
	}

	public override void ExitState()
	{
		Debug.Log("FinalPuzzleStory End");
	}

	public override void Update()
	{
	
	}

}