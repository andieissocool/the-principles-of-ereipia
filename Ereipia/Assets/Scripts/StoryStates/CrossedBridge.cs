﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CrossedBridge: StoryState
{
	public CrossedBridge(StoryMachine storyMachine, GameManager gameManager) : base(storyMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("CrossedBridge Start");
		gameManager.storyState4.SetActive(true);
	}

	public override void ExitState()
	{
		Debug.Log("CrossedBridge End");
		gameManager.storyState4.SetActive(false);
	}

	public override void Update()
	{

		if (gameManager.puzzleCounter == 3)
        {
			gameManager.StoryM.ChangeState(gameManager.finalPuzzleStory);
        }
	}

}