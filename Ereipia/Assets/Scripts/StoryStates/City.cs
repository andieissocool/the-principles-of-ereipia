﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : StoryState
{
   
    public City(StoryMachine storyMachine, GameManager gameManager) : base(storyMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("city");
	}

	public override void ExitState()
	{
		gameManager.storySTate2.SetActive(false);
	}

	public override void Update()
	{
        if(gameManager.triggerState02 == 1)
        {
            gameManager.StoryM.ChangeState(gameManager.labyrinthStory);
        }
	}
}
