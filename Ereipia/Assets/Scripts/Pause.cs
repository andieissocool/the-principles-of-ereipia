﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("Pause Screen loaded");
    }

    void OnGUI()
    {
        if (Input.GetKeyDown("p"))
        {
            SceneManager.LoadScene("Landmass");
        }
    }

}
