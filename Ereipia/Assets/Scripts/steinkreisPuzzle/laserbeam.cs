﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserbeam : MonoBehaviour
{

    public GameManager gameManager;
    private LineRenderer lineR;

    public Transform start;
    public Transform end;

    public float lineDrawSpeed = 6f;
    private float counter = 0;
    private float distance;

    void Start()
    {
        lineR = GetComponent<LineRenderer>();
        lineR.SetPosition(0, start.position);
        lineR.SetWidth(5f, 5f);

        distance = Vector3.Distance(start.position, end.position);
    }

    void Update()
    {
        if (gameManager.correctRows == true)
        {

            lineR.SetWidth(10000f, 10000f);

            if (counter < distance)
            {
                counter += .10f / lineDrawSpeed;
                float x = Mathf.Lerp(0, distance, counter);

                Vector3 pointA = start.position;
                Vector3 pointB = end.position;
                Vector3 pointLine = x * Vector3.Normalize(pointB - pointA) + pointA;

                lineR.SetPosition(1, pointLine);
            }

        } else
        {
            lineR.SetWidth(0f, 0f);
        }
    }

}
