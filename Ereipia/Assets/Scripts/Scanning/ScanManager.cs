﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanManager : MonoBehaviour
{
    [SerializeField]
    private string selectableTag = "Selectable";    // gives the object a tag so the system knows what object can be selected

    [SerializeField]
    private string interactableTag = "Selectable";  // for interaction object

    // keep track of the current selection
    private Transform currentSelection;

    [Header("GameManager")]
    public GameManager gameManager;

    [Header("GUI")]
    public GameObject textGUI;
    public Text keyText;
    public Text keyDescription;

    [Header("Loading")]
    public GameObject loadingMenu;
    public GameObject loadingScript;
    public Image scanImage;

    [Header("Subtitle")]
    public GameObject guiSubtitle;
    public GameObject subtitleObj;

    private float progress;

    private void Update()
    {
        if (gameManager.SM.CurrentState == gameManager.deviceScan)
        {

            if (currentSelection != null)
            {
                var selectionRenderer = currentSelection.GetComponent<Renderer>();

                selectionRenderer.material = currentSelection.GetComponent<runeTarget>().defaultMaterial;
                currentSelection = null;
                textGUI.SetActive(false);
            }

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit target;

            if (Physics.Raycast(ray, out target))
            {
                var selection = target.transform;
                if (selection.CompareTag(selectableTag))
                {
                    var selectionRenderer = selection.GetComponent<Renderer>();
                    if (selectionRenderer != null)
                    {
                        selectionRenderer.material = selection.GetComponent<runeTarget>().highlightMaterial;

                        textGUI.SetActive(true);

                        keyText.text = selection.GetComponent<runeTarget>().keyString;
                        keyDescription.text = selection.GetComponent<runeTarget>().descriptionString;

                        if (Input.GetKeyDown("r"))
                        {
                            selection.GetComponent<runeTarget>().guardianRune.active = true;
                            scanImage.sprite = selection.GetComponent<runeTarget>().guardianRune.runeScanImg;

                            textGUI.SetActive(false);

                            loadingScript.GetComponent<guiAnimation>().setProgress(0);
                            loadingMenu.SetActive(true);

                            StartCoroutine(wait());
                        }
                    }

                    currentSelection = selection;
                }
            }
        }
    }

    IEnumerator wait()
    {
        for (float i = 0; i <= 1.1;)
        {
            yield return new WaitForSeconds(.01f);
            loadingScript.GetComponent<guiAnimation>().setProgress(i);
            i = i + .02f;

            progress = loadingScript.GetComponent<guiAnimation>().getProgress();
        }

        textGUI.SetActive(false);
        loadingMenu.SetActive(false);
        gameManager.SM.ChangeState(gameManager.peaMenu);
        gameManager.peaMainMenu.SetActive(false);
        gameManager.guardiansMenu.SetActive(true);
    }
}
