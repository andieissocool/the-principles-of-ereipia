﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public float viewRadius;
    [Range(0,360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    //[HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();
    
    private void Start()
    {
        // iniziate corutine
        StartCoroutine("FindTargetsWithDelay", .2f);    // coroutine is started every 0.2 seconds
        Debug.Log("FieldOfView Start");
    }
    
    // call FindVisibleTargets from a coroutine
    IEnumerator FindTargetsWithDelay(float delay)
    {
        while(true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    void FindVisibleTargets()
    {
        // clear visible Targets list at start --> for no duplicates
        visibleTargets.Clear();

        // get collieder of all targets that should interact with the view --> basically all highlighted targets
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if(Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2) // target is within our view angle
            {
                // see if there is an obstacle between charakter and target
                float dstToTarget = Vector3.Distance(transform.position, target.position);

                // if we don't collide with anything when we perform a raycast
                if(!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask)) {
                    // no obstacles are in the way --> we can transform target
                    // enter here what u want to do with the target --> highlight outlines in blue

                    target.GetComponent<Outline>().enabled = true;
                    Debug.Log("target from FieldOfView: " + target);

                    visibleTargets.Add(target);
                }
            }
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if(!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

   void OnDisable()
    {
        Debug.Log("disable");

        foreach (Transform target in visibleTargets)
        {
            StopCoroutine("FindTargetsWithDelay");
            target.GetComponent<Outline>().enabled = false;
        }
    }

    void OnEnable()
    {
        StartCoroutine("FindTargetsWithDelay", .2f);
    }
}
