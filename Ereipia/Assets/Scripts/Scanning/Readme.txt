Scanning

to add objects to the raycast scanning:
- select the object
- set ScanTarget as Layer
- give the Object the script Outline from QuickOutline - Scripts
- change following:
         Outline Mode: Outline Visible
         Outline Color: Cyan (Swatches) (00FFFF)
         Outline Width: 1
- disable the script

to add objects to obstacles (scan won't go through)
- select the object
- set Obstacle as Layer