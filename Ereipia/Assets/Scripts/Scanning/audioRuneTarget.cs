﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class audioRuneTarget : MonoBehaviour
{
    [Header("state Machine")]
    public GameObject gameManager;

    [Header("Rune objects")]
    public runeObject audioRune;
    public GameObject runeMenu;

    [Header("GUI Anzeigetext")]
    public string keyString;
    public string descriptionString;

}
