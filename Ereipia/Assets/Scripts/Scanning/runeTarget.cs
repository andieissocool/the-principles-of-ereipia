﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class runeTarget : MonoBehaviour
{
    [Header("state Machine")]
    public GameObject gameManager;

    [Header("guardian objects")]
    public guardiansObject guardianRune;
    public GameObject guardianMenu;

    [Header("material")]
    public Material highlightMaterial;
    public Material defaultMaterial;

    [Header("GUI Anzeigetext")]
    public string keyString;
    public string descriptionString;

}
