﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanManagerAudioPuzzle : MonoBehaviour
{
    [SerializeField]
    private string selectableTag = "Selectable";    // gives the object a tag so the system knows what object can be selected

    // keep track of the current selection
    private Transform currentSelection;

    [Header("GameManager")]
    public GameManager gameManager;

    [Header("GUI")]
    public GameObject textGUI;
    public Text keyText;
    public Text keyDescription;

    [Header("Loading")]
    public GameObject loadingMenu;
    public GameObject scanImage;
    public GameObject loadingScript;

    public Sprite empty;

    private float progress;

    private void Update()
    {
        if (gameManager.SM.CurrentState == gameManager.deviceScan)
        {

            if (currentSelection != null)
            {
                var selectionRenderer = currentSelection.GetComponent<Renderer>();

                currentSelection = null;
                textGUI.SetActive(false);
            }

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit target;

            if (Physics.Raycast(ray, out target))
            {
                var selection = target.transform;
                if (selection.CompareTag(selectableTag) && selection.GetComponent<audioRuneTarget>())
                {
                    var selectionRenderer = selection.GetComponent<Renderer>();
                    if (selectionRenderer != null)
                    {
                        textGUI.SetActive(true);

                        keyText.text = selection.GetComponent<audioRuneTarget>().keyString;
                        keyDescription.text = selection.GetComponent<audioRuneTarget>().descriptionString;

                        if (Input.GetKeyDown("r"))
                        {
                            selection.GetComponent<audioRuneTarget>().audioRune.active = true;
                            scanImage.GetComponent<Image>().sprite = selection.GetComponent<audioRuneTarget>().audioRune.ScanImg;

                            textGUI.SetActive(false);

                            loadingScript.GetComponent<guiAnimation>().setProgress(0);
                            loadingMenu.SetActive(true);

                            StartCoroutine(wait());
                        }
                    }

                    currentSelection = selection;
                }
                else if(selection.CompareTag(selectableTag) && selection.GetComponent<Decipher>())
                {
                    var selectionRenderer = selection.GetComponent<Renderer>();
                    if (selectionRenderer != null)
                    {
                        textGUI.SetActive(true);

                        keyText.text = selection.GetComponent<Decipher>().keyString;
                        keyDescription.text = selection.GetComponent<Decipher>().descriptionString;

                        if (Input.GetKeyDown("r"))
                        {
                            scanImage.GetComponent<Image>().sprite = empty;

                            textGUI.SetActive(false);
                            loadingScript.GetComponent<guiAnimation>().setProgress(0);
                            loadingMenu.SetActive(true);

                            StartCoroutine(deciphering());
                        }
                    }
                    currentSelection = selection;
                }
            }
        }
    }

    IEnumerator wait()
    {
        for (float i = 0; i <= 1.1;)
        {
            yield return new WaitForSeconds(.01f);
            loadingScript.GetComponent<guiAnimation>().setProgress(i);
            i = i + .02f;

            progress = loadingScript.GetComponent<guiAnimation>().getProgress();
        }

        textGUI.SetActive(false);
        loadingMenu.SetActive(false);
        gameManager.SM.ChangeState(gameManager.peaMenu);
        gameManager.peaMainMenu.SetActive(false);
        gameManager.runesMenu.SetActive(true);
        gameManager.runesMenu.GetComponent<runesMenu>().runesSlider.SetActive(false);
        gameManager.runesMenu.GetComponent<runesMenu>().audioRunesSlider.SetActive(true);
    }

    IEnumerator deciphering()
    {
        for (float i = 0; i <= 1.1;)
        {
            yield return new WaitForSeconds(.01f);
            loadingScript.GetComponent<guiAnimation>().setProgress(i);
            i = i + .02f;

            progress = loadingScript.GetComponent<guiAnimation>().getProgress();
        }
        loadingMenu.SetActive(false);
        gameManager.Decipher();
    }
}
