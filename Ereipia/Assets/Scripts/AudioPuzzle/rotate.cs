﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject Symbols;
    public GameObject Crystal;
    public GameObject left;
    public GameObject right;
    public GameObject LightningBall;
    public GameObject Afterburner;
    public GameObject CrystalSound;

    private void OnTriggerEnter(Collider other)
    {
        if (this.name == "left" && gameManager.activated == 0)
        {
            this.transform.position += new Vector3(0, (float)-0.15, 0);
            left.transform.position += new Vector3(0, (float)-0.15, 0);
            Symbols.transform.Rotate(0, 10, 0);
            Symbols.GetComponent<AudioSource>().Play();
            isAlligned();
        }
        else if(this.name == "right" && gameManager.activated == 0)
        {
            this.transform.position += new Vector3(0, (float)-0.15, 0);
            right.transform.position += new Vector3(0, (float)-0.15, 0);
            Symbols.transform.Rotate(0, -10, 0);
            Symbols.GetComponent<AudioSource>().Play();
            isAlligned();
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (this.name == "left" && gameManager.activated == 0)
        {
            this.transform.position += new Vector3(0, (float)0.15, 0);
            left.transform.position += new Vector3(0, (float)0.15, 0);
        }
        else if (this.name == "right" && gameManager.activated == 0)
        {
            this.transform.position += new Vector3(0, (float)0.15, 0);
            right.transform.position += new Vector3(0, (float)0.15, 0);
        }
    }

    private void isAlligned()
    {
        if((int)Symbols.transform.eulerAngles.y  == 259 || Symbols.transform.eulerAngles.y == 260)
        {
            gameManager.activated = 1;
            this.transform.position += new Vector3(0, (float)0.15, 0);
            right.transform.position += new Vector3(0, (float)0.15, 0);
            Crystal.GetComponent<Animator>().SetBool("alligend", true);
            StartCoroutine("CrystalActivated");
        
        }

    }

    IEnumerator CrystalActivated()
    {
  
        gameManager.puzzleCounter++;
        CrystalSound.GetComponent<AudioSource>().Play();
        LightningBall.GetComponent<ParticleSystem>().Play();
        Afterburner.GetComponent<ParticleSystem>().Play();

        gameManager.subtibtleMenu.SetActive(true);
        gameManager.subtitleText.text = gameManager.AudioPuzzlesubtitle2;

        yield return new WaitForSeconds(8);

        gameManager.subtibtleMenu.SetActive(false);
        gameManager.saveObject.GetComponent<SaveGame>().Save();

    }
}

