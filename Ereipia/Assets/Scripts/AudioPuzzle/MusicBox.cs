﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBox : MonoBehaviour
{
    public GameObject MusikBox;
    public GameManager gameManager;

    private void OnTriggerEnter(Collider other)
    {
        gameManager.tilesAudio.GetComponent<AudioSource>().Play();
        MusikBox.GetComponent<AudioSource>().Stop();
    }

    private void OnTriggerExit(Collider other)
    {
        MusikBox.GetComponent<AudioSource>().Play();
    }

}
