﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tunes : MonoBehaviour
{
    public GameManager gameManager;
    private Sprite NAME;

    void Start()
    {
        NAME = Resources.Load<Sprite>(this.name);
    }

    //When the Primitive collides with the walls, it will reverse direction
    private void OnTriggerEnter(Collider other)
    {
        AudioSource clip = this.GetComponent<AudioSource>();
        gameManager.tilesAudio.GetComponent<AudioSource>().Play();
        this.transform.position += new Vector3(0, (float)-0.15,0);
        for (int i = 0; i < gameManager.ImagesTiles.Length; i++)
        {
            if (gameManager.ImagesTiles[i].name == this.name)
            {
                gameManager.ImagesTiles[i].transform.position += new Vector3(0, (float)-0.15, 0);
            }
        }
        if (gameManager.code.Count == 6 && gameManager.result != "finished")
        {
            gameManager.clearImages();
            gameManager.code.Clear();
            gameManager.counter = 0;
        }
        if (gameManager.counter == 0)
        {
            gameManager.clearImages();
        }

        if (this.name == "reset")
        {
            gameManager.clearImages();
            gameManager.code.Clear();
            gameManager.counter = 0;

            for (int i = 0; i < gameManager.code.Count; i++)
            {
                gameManager.code.Remove(gameManager.code[i]);
            }

        }
        else if (this.name == "undo")
        {
            if (gameManager.counter != 0) { 
                gameManager.counter--;
                gameManager.Images[gameManager.counter].GetComponent<Image>().sprite = gameManager.empty;
                gameManager.code.RemoveAt(gameManager.code.Count - 1);
            }
        }
        else if (gameManager.result == "finished")
        {
            clip.Play();
        }
        else
        {
            clip.Play();
            gameManager.code.Add(this.name);
            gameManager.result = string.Join("", gameManager.code);

            if (gameManager.counter < 5) {
                gameManager.Images[gameManager.counter].GetComponent<Image>().sprite = NAME;
                gameManager.counter++;
            }
            else
            {
                gameManager.Images[gameManager.counter].GetComponent<Image>().sprite = NAME;
                gameManager.counter++;
            }

        }

    }

    private void OnTriggerExit(Collider other)
    {
        this.transform.position += new Vector3(0, (float)0.15, 0);
        for (int i = 0; i < gameManager.ImagesTiles.Length; i++)
        {
            if (gameManager.ImagesTiles[i].name == this.name)
            {
                gameManager.ImagesTiles[i].transform.position += new Vector3(0, (float)0.15, 0);
            }
        }
        if (this.name != "reset") {
            AudioSource clip = this.GetComponent<AudioSource>();
            clip.Stop();
        }
    }

}
