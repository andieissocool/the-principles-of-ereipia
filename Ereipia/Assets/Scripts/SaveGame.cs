﻿using System.Collections;
using System.Collections.Generic;
using GAD;
using KinematicCharacterController;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveGame: MonoBehaviour
{
    public GameManager gameManager;
    public GameObject character;
    Vector3 temp;
    Vector3 temp2;

    public void Save()
    {
        PlayerPrefs.SetInt("loadAudioPuzzle", gameManager.enteringUndergroundPuzzle);
        PlayerPrefs.SetInt("counterAudioPuzzle", gameManager.counter);
        PlayerPrefs.SetString("resultAudioPuzzle", gameManager.result);
        PlayerPrefs.SetInt("AudioPuzzleCrystalAktivated", gameManager.activated);

        PlayerPrefs.SetInt("Counter", gameManager.puzzleCounter);
        PlayerPrefs.SetString("ActiveScene", SceneManager.GetActiveScene().name);

        PlayerPrefs.SetString("StoryState", gameManager.StoryM.CurrentState.ToString());

        if (SceneManager.GetActiveScene().name == "AudioPuzzle")
        {
            PlayerPrefs.SetFloat("AudioCharachterX", character.transform.position.x);
            PlayerPrefs.SetFloat("AudioCharachterY", character.transform.position.y);
            PlayerPrefs.SetFloat("AudioCharachterZ", character.transform.position.z);
            PlayerPrefs.SetFloat("AudioRotationY", character.transform.rotation.y);

            PlayerPrefs.SetString("AudioState", gameManager.SM.CurrentState.ToString());
        } else
        {
            PlayerPrefs.SetFloat("CharachterX", character.transform.position.x);
            PlayerPrefs.SetFloat("CharachterY", character.transform.position.y);
            PlayerPrefs.SetFloat("CharachterZ", character.transform.position.z);
            PlayerPrefs.SetFloat("RotationY", character.transform.rotation.y);

            PlayerPrefs.SetString("State", gameManager.SM.CurrentState.ToString());

        }

    }


    public void Load()
    {
        gameManager.enteringUndergroundPuzzle = PlayerPrefs.GetInt("loadAudioPuzzle");
        gameManager.counter = PlayerPrefs.GetInt("counterAudioPuzzle");
        gameManager.result = PlayerPrefs.GetString("resultAudioPuzzle");
        gameManager.activated = PlayerPrefs.GetInt("AudioPuzzleCrystalAktivated");

        gameManager.puzzleCounter = PlayerPrefs.GetInt("Counter");

        foreach (StoryState story in gameManager.Story)
        {
            if (story.ToString() == PlayerPrefs.GetString("StoryState"))
            {
                gameManager.StoryM.ChangeState(story);
            }
        }

        if (SceneManager.GetActiveScene().name == "Landmass")
        {
            temp = new Vector3(PlayerPrefs.GetFloat("CharachterX")-1, PlayerPrefs.GetFloat("CharachterY"), PlayerPrefs.GetFloat("CharachterZ")-1);
            character.GetComponent<KinematicCharacterMotor>().SetPosition(temp);

            Quaternion temp3 = new Quaternion(character.transform.rotation.x, PlayerPrefs.GetFloat("RotationY"), character.transform.rotation.z, character.transform.rotation.w);
            character.GetComponent<KinematicCharacterMotor>().SetRotation(temp3);

            foreach (State state in gameManager.State)
            {
                if (state.ToString() == PlayerPrefs.GetString("State"))
                {
                    if (state == gameManager.staticMenue)
                    {
                        gameManager.SM.ChangeState(gameManager.walkingState);
                    }
                    else
                    {
                        gameManager.SM.ChangeState(state);
                    }
            
                }
            }

        }
        else
        {
            if (PlayerPrefs.GetInt("loadAudioPuzzle") == 1)
            {
                temp2 = new Vector3(715.6f, 926.7f, -215.8f);
                character.GetComponent<KinematicCharacterMotor>().SetPosition(temp2);
            } else
            {
                temp2 = new Vector3(PlayerPrefs.GetFloat("AudioCharachterX"), PlayerPrefs.GetFloat("AudioCharachterY"), PlayerPrefs.GetFloat("AudioCharachterZ"));
                character.GetComponent<KinematicCharacterMotor>().SetPosition(temp2);
            }
            Quaternion temp3 = new Quaternion(character.transform.rotation.x, PlayerPrefs.GetFloat("AudioRotationY"), character.transform.rotation.z, character.transform.rotation.w);
            character.GetComponent<KinematicCharacterMotor>().SetRotation(temp3);

            foreach (State state in gameManager.State)
            {
                if (state.ToString() == PlayerPrefs.GetString("AudioState"))
                {
                    if (state == gameManager.staticMenue)
                    {
                        gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
                    }
                    else
                    {
                        gameManager.SM.ChangeState(state);
                    }
                }
            }

        }
    }
}
