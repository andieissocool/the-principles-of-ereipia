﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showHints : MonoBehaviour
{
    public GameObject hints;
    public GameObject text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter() {
        Text texti = text.GetComponent<Text>();
        texti.text = "Press 'u' to show hints";
        hints.SetActive(true);
        StartCoroutine(Waiting(3f));
    }

    IEnumerator Waiting(float wait)
    {
        yield return new WaitForSeconds(wait);
        hints.SetActive(false);
    }
}
