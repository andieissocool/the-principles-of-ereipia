﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//final puzzle

public class keyspressed : MonoBehaviour
{
    public GameObject[] keys;
    public GameObject scanVolume;
    public GameObject steinkreis;
    public bool triggerTemple;
    public AudioSource audioSource;

    private int circleOnePressedCounter;
    private int circleTwoPressedCounter;
    private int circleThreePressedCounter;
    private int circleFourPressedCounter;

    [SerializeField] private Animator stairAnimation;
    [SerializeField] private Animator stairAnimation02;
    [SerializeField] private Animator stairAnimation03;
    [SerializeField] private Animator stairAnimation04;
    [SerializeField] private Animator floatyThing;

    void Start()
    {
        circleOnePressedCounter = 0;
        circleTwoPressedCounter = 0;
        circleThreePressedCounter = 0;
        circleFourPressedCounter = 0;
    }

    
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //mit switch ersetzten!!!
        if (other.tag == "circle01")
        {
            if (circleOnePressedCounter >= 1)
            {
                resetButton("circle01");
            }
            other.GetComponent<key>().pressKey();
            circleOnePressedCounter++;
        }

        if (other.tag == "circle02")
        {
            if (circleTwoPressedCounter >= 1)
            {
                resetButton("circle02");
            }
            other.GetComponent<key>().pressKey();
            circleTwoPressedCounter++;
        }

        if (other.tag == "circle03")
        {
            if(circleThreePressedCounter >= 1)
            {
                resetButton("circle03");
            }
            other.GetComponent<key>().pressKey();
            circleThreePressedCounter++;
        }

        if (other.tag == "circle04")
        {
            if (circleFourPressedCounter >= 1)
            {
                resetButton("circle04");
            }
            other.GetComponent<key>().pressKey();
            circleFourPressedCounter++;
        }

        if(other.name == "triggerlastpuzzle")
        {
            scanVolume.SetActive(true);
            steinkreis.SetActive(true);
        }

        if (other.name == "triggerTemple")
        {
            triggerTemple = true;
        }

        //print(checkRuneCombination());
        if (checkRuneCombination() == 4)
        {
            stairAnimation.SetBool("revealRoom", true);
            stairAnimation02.SetBool("revealRoom", true);
            stairAnimation03.SetBool("revealRoom", true);
            stairAnimation04.SetBool("revealRoom", true);
            floatyThing.SetBool("revealRoom", true);
            audioSource.Play();
            scanVolume.SetActive(false);
            steinkreis.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "triggerlastpuzzle")
        {
            scanVolume.SetActive(false);
            steinkreis.SetActive(false);
        }

        if (other.name == "triggerTemple")
        {
            triggerTemple = false;
        }
    }

    private void resetButton(string tag)
    {
        keys = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject key in keys)
        {
            if(key.GetComponent<key>().pressed == true)
            {
                key.GetComponent<key>().resetKey();
            }
        }
    }

    private int checkRuneCombination()
    {
        int counterCorrectRunes = 0;

        keys = GameObject.FindGameObjectsWithTag("circle01");
        foreach (GameObject key in keys)
        {
            if (key.GetComponent<key>().pressed == true && key.name == "earth") 
            {
                counterCorrectRunes++;
            }
        }

        keys = GameObject.FindGameObjectsWithTag("circle02");
        foreach (GameObject key in keys)
        {
            if (key.GetComponent<key>().pressed == true && key.name == "storm") 
            {
                counterCorrectRunes++;
            }
        }

        keys = GameObject.FindGameObjectsWithTag("circle03");
        foreach (GameObject key in keys)
        {
            if (key.GetComponent<key>().pressed == true && key.name == "mechanics") 
            {
                counterCorrectRunes++;
            }
        }

        keys = GameObject.FindGameObjectsWithTag("circle04");
        foreach (GameObject key in keys)
        {
            if (key.GetComponent<key>().pressed == true && key.name == "magic") 
            {
                counterCorrectRunes++;
            }
        }
        return counterCorrectRunes;
    }
}
