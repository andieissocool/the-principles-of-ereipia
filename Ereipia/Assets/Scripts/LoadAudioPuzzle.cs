﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadAudioPuzzle : MonoBehaviour
{
    public GameObject saveObject;
    public GameManager gameManager;

    private void OnTriggerEnter(Collider other)
    {
        if (SceneManager.GetActiveScene().name == "Landmass")
        {
            gameManager.enteringUndergroundPuzzle = 0;
            saveObject.GetComponent<SaveGame>().Save();
            SceneManager.LoadScene("AudioPuzzle");
        } else
        {
            saveObject.GetComponent<SaveGame>().Save();
            SceneManager.LoadScene("Landmass");
        }
    }
}
