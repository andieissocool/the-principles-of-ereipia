﻿using System.Collections;

public abstract class StoryState
{
    protected StoryMachine storyMachine;
    protected GameManager gameManager;

    protected StoryState(StoryMachine storyMachine, GameManager gameManager)
    {
        this.storyMachine = storyMachine;
        this.gameManager = gameManager;
    }

    public virtual void EnterState()
    {
 
    }

    public virtual void ExitState()
    {
        
    }

    public virtual void Update()
    {
       
    }

}
