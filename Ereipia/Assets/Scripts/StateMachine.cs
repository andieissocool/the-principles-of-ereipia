﻿using System.Collections;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public State CurrentState;

    public void Initialize(State state)
    {
        CurrentState = state;
        state.EnterState();
    }

    public void ChangeState(State newState)
    {
        CurrentState.ExitState();

        CurrentState = newState;
        newState.EnterState();
    }
}
