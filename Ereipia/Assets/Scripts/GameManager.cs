﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using KinematicCharacterController;

public class GameManager : MonoBehaviour
{
    public GameObject saveObject;

    [Header("State Machine")]
    public StateMachine SM;

    [Header("Story Machine")]
    public StoryMachine StoryM;

    [Header("State Classes")]
    public List<State> State = new List<State>();
    public WalkingState walkingState;
    public LabyrinthPuzzle labyrinthPuzzle;
    public RoatingPuzzle roatingPuzzle;
    public UndergroundPuzzle undergroundPuzzle;
    public FinalPuzzle finalPuzzle;
    public DeviceScan deviceScan;

    [Header("StoryState Classes")]
    public List<StoryState> Story = new List<StoryState>();
    public Beginning begining;
    public City city;
    public CrossedBridge crossedBridge;
    public LabyrinthStory labyrinthStory;
    public FinalPuzzleStory finalPuzzleStory;

    [Header("Static Interface States")]
    public StaticMenueState staticMenue; //pause Menu
    public PeaState peaMenu;

    [Header("Static Inerface Variables")]

    /*needed for all static gui elements*/
    public GameObject fpsControllerGroup;
    public GameObject guiCam;        
    public GameObject guiEventSystem;

    /*pauseMenue*/
    public GameObject guiPause;
    public GameObject bee1;
    public GameObject bee2;
    public GameObject bee3;

    [Header("PEA")]
    public GameObject peaMainMenu;
    public GameObject msgMenu;
    public GameObject runesMenu;
    public GameObject logMenu;
    public GameObject GUI;

    [Header("einblendtext")]
    public GameObject interactionMenu;
    public Text keyLetter;
    public Text keyDescription;

    [Header("msgMenu Parts")]
    public GameObject generalButton;
    public GameObject generalMenu;
    public GameObject mcButton;
    public GameObject mcMenu;
    public GameObject avoryButton;
    public GameObject avoryMenu;
    public GameObject generalTextCont;
    public Text generalHL;
    public Text generalText;
    public GameObject generalWithPicTextCont;
    public Text generalPicHL;
    public Text generalPicText;
    public Image generalPic;
    
    [Header("Overlay GUI")]
    public GameObject mittelkreuz;
    public GameObject guardiansMenu;
    public GameObject newLog;
    public GameObject newRepairedLog;

    [Header("Subtitle GUI")]
    public GameObject subtibtleMenu;
    public Text subtitleText;

    [Header("Anfang GUI")]
    public bool firstTimeDone;
    public bool firstRepairStart;
    public GameObject openMenu;
    public GameObject warningMenu;

    [Header("Repairs")]
    public GameObject repairObj;
    public Text repairText;
    public AudioClip peaDamaged;
    public bool peaDamagedPlayed;
    public float Volume;

    [Header("story parts")]
    public GameObject storyState1;
    public GameObject storySTate2;
    public GameObject storyState4;

    [Header("trigger story part")]
    public int triggerState02;

    [Header("Steinkreisrätsel")]
    public bool steinkreis_active;
    public GameObject steinkreisGUI;
    public GameObject steinkreisVolume;
    public GameObject[] rows = new GameObject[4];
    public GameObject[] rows_mat = new GameObject[4];
    public bool correctRows = false;
    public bool correctRow01 = false;
    public bool correctRow02 = false;
    public bool correctRow03 = false;
    public bool correctRow04 = false;
    public Material[] materials_rows_start = new Material[4];
    public Material[] materials_rows_selected = new Material[4];
    public Material[] materials_rows_final = new Material[4];
    public GameObject laser_01;
    public GameObject laser_02;

    [Header("Center Highlight")]
    public GameObject center;

    [Header("Audio Puzzle")]
    public int enteringUndergroundPuzzle = 0;
    public GameObject LightningBall;
    public GameObject Afterburner;
    public GameObject CrystalSound;
    public List<string> code;
    public string result = "";
    public GameObject door;
    public GameObject doorAudio;
    public GameObject doorAudio2;
    public GameObject tilesAudio;
    public int counter = 0;
    public GameObject[] Images;
    public GameObject[] ImagesTiles;
    public Sprite empty;
    public int activated = 0;
    private Sprite DECIPHERED;
    public string AudioPuzzlesubtitle = "";
    public string AudioPuzzlesubtitle2 = "";

    [Header("Scan Function")]
    public GameObject scanVolume;
    public GameObject character;

    [Header("Guardian Animation")]
    public GameObject guardian1ani;
    public GameObject guardian2ani;
    public GameObject guardian3ani;
    public GameObject guardian4ani;

    [Header("Final Puzzle")]
    public int puzzleCounter;
    public GameObject colliderfinalpuzzle;
    public GameObject blitze;
    public GameObject kristall;
    public GameObject triggerlastpuzzle;

    private void Start()
    {
        SM = new StateMachine();
        StoryM = new StoryMachine();

        walkingState = new WalkingState(SM, this);
        labyrinthPuzzle = new LabyrinthPuzzle(SM, this);
        roatingPuzzle = new RoatingPuzzle(SM, this);
        undergroundPuzzle = new UndergroundPuzzle(SM, this);
        finalPuzzle = new FinalPuzzle(SM, this);
        deviceScan = new DeviceScan(SM, this);
        staticMenue = new StaticMenueState(SM, this);
        peaMenu = new PeaState(SM, this);

        State.Add(walkingState);
        State.Add(labyrinthPuzzle);
        State.Add(undergroundPuzzle);
        State.Add(finalPuzzle);
        State.Add(deviceScan);
        State.Add(staticMenue);
        State.Add(peaMenu);

        begining = new Beginning(StoryM, this);
        city = new City(StoryM, this);
        labyrinthStory = new LabyrinthStory(StoryM, this);
        crossedBridge = new CrossedBridge(StoryM, this);
        finalPuzzleStory = new FinalPuzzleStory(StoryM, this);

        Story.Add(begining);
        Story.Add(city);
        Story.Add(labyrinthStory);
        Story.Add(crossedBridge);
        Story.Add(finalPuzzleStory);

        StoryM.Initialize(begining);
        SM.Initialize(walkingState);
        if (SceneManager.GetActiveScene().name == "AudioPuzzle")
        {
            SM.Initialize(undergroundPuzzle);
        }
        saveObject.GetComponent<SaveGame>().Load();
    }

    private void Update()
    {
        SM.CurrentState.Update();
        StoryM.CurrentState.Update();

    }

    public void clearImages()
    {
        for (int i = 0; i < Images.Length; i++)
        {
            Images[i].GetComponent<Image>().sprite = empty;
        }

    }

    public void Decipher()
    {
        for (int i = 0; i < Images.Length; i++)
        {
            DECIPHERED = Resources.Load<Sprite>("letters/" + code[i]);
            Images[i].GetComponent<Image>().sprite = DECIPHERED;
        }
    }

    public void Encode()
    {
        for (int i = 0; i < code.Count; i++)
        {
            DECIPHERED = Resources.Load<Sprite>(code[i]);
            Images[i].GetComponent<Image>().sprite = DECIPHERED;
        }
    }
}
