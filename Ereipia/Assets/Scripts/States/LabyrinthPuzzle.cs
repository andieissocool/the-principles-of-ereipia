﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LabyrinthPuzzle : State
{
	public LabyrinthPuzzle(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("LabyrinthPuzzle loaded");
	}

	public override void ExitState()
	{
		Debug.Log("Pause game");
	}

	public override void Update()
	{

		if (Input.GetKeyDown("p"))
		{
			gameManager.SM.ChangeState(gameManager.staticMenue);
		}

	}
}
