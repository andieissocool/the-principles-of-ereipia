﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using KinematicCharacterController;
using GAD;

public class StaticMenueState : State
{
	public StaticMenueState(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("pauseState - staticMenueState started");

		/*deactivate fpsControlelr*/
		gameManager.fpsControllerGroup.SetActive(false);

		/*deactivate bee scripts of fpsControlelr*/
		gameManager.bee1.GetComponent<Bees>().enabled = false;
		gameManager.bee2.GetComponent<Bees>().enabled = false;
		gameManager.bee3.GetComponent<Bees>().enabled = false;

		/*set cursor active*/
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;

		/*set static gui elements active*/
		gameManager.GUI.SetActive(true);
		gameManager.guiCam.SetActive(true);
		gameManager.guiEventSystem.SetActive(true);

		/*set menu active*/
		gameManager.guiPause.SetActive(true);
	}

	public override void ExitState()
	{
		Debug.Log("pauseState - staticMenueState ended");

		/*reactivate fpsControlelr*/
		gameManager.fpsControllerGroup.SetActive(true);

		/*reactivate bee scripts*/
		gameManager.bee1.GetComponent<Bees>().enabled = true;
		gameManager.bee2.GetComponent<Bees>().enabled = true;
		gameManager.bee3.GetComponent<Bees>().enabled = true;

		/*deactivate cursor*/
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;

		/*deactivate static gui elements*/
		gameManager.GUI.SetActive(false);
		gameManager.guiEventSystem.SetActive(false);
		gameManager.guiCam.SetActive(false);

		/*deactivate menu active*/
		gameManager.peaMainMenu.SetActive(false);
		gameManager.msgMenu.SetActive(false);
		gameManager.runesMenu.SetActive(false);
		gameManager.guardiansMenu.SetActive(false);
		gameManager.logMenu.SetActive(false);
		gameManager.GUI.SetActive(false);
	}

	public override void Update()
	{
		if (Input.GetKeyDown("c") && SceneManager.GetActiveScene().name == "AudioPuzzle")
		{
			gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
        }
        if (Input.GetKeyDown("c") && SceneManager.GetActiveScene().name == "Landmass")
        {
			gameManager.SM.ChangeState(gameManager.walkingState);
		}
	}

	public void continueGame() {
		gameManager.peaMainMenu.SetActive(false);
		gameManager.msgMenu.SetActive(false);
		gameManager.runesMenu.SetActive(false);
		gameManager.guardiansMenu.SetActive(false);
		gameManager.logMenu.SetActive(false);
		gameManager.GUI.SetActive(false);

        if (SceneManager.GetActiveScene().name == "AudioPuzzle")
        {
			gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
		}
        else
        {
			gameManager.SM.ChangeState(gameManager.walkingState);
		}
	}

}