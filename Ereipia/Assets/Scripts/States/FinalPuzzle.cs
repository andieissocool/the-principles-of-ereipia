﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalPuzzle : State
{
	public FinalPuzzle(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

    

    public override void EnterState()
	{
		Debug.Log("finalPuzzleSTATE");
        if(gameManager.puzzleCounter == 3)
        {
            gameManager.colliderfinalpuzzle.SetActive(false);
            gameManager.blitze.SetActive(false);
            gameManager.kristall.SetActive(true);
            gameManager.triggerlastpuzzle.SetActive(true);
        }
	}
   
    public override void ExitState()
	{
		Debug.Log("Pause game");
	}

	public override void Update()
	{

        if (Input.GetKeyDown("p"))
        {
            gameManager.SM.ChangeState(gameManager.staticMenue);
        }

        if (gameManager.character.GetComponent<keyspressed>().triggerTemple == false)
        {
            gameManager.SM.ChangeState(gameManager.walkingState);
        }

    }

}
