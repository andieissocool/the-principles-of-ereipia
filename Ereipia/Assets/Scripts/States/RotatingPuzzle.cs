﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoatingPuzzle : State
{
	public RoatingPuzzle(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

    private int indexRows = 0;

    private GameObject[] steinkreise = new GameObject[4];
    private GameObject[] steinkreise_mat = new GameObject[4];
    private bool fullMat = false;

    private GameObject laser01;
    private GameObject laser02;

    public override void EnterState()
	{
		Debug.Log("RoatingPuzzle loaded");

    }

    public override void ExitState()
	{
		Debug.Log("Pause game");
	}

    public override void Update()
	{

        if (Input.GetKeyDown("p"))
        {
            gameManager.SM.ChangeState(gameManager.staticMenue);
        }

        if (!gameManager.steinkreis_active)
        {
            gameManager.SM.ChangeState(gameManager.walkingState);
        }

        for (int i = 0; i < 4; i++)
        {
            steinkreise[i] = gameManager.rows[i];
        }

        if (fullMat == false)
        {
            for (int i = 0; i < 4; i++)
            {
                steinkreise_mat[i] = gameManager.rows_mat[i];
            }

            fullMat = true;
            steinkreise_mat[indexRows].GetComponent<MeshRenderer>().material = gameManager.materials_rows_selected[indexRows];
            
            laser01 = gameManager.laser_01;
            laser02 = gameManager.laser_02;
            laser01.SetActive(false);
            laser02.SetActive(false);
        }


        if (gameManager.correctRows == false)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                RotateRight();
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                RotateLeft();
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                SwitchRows();
            }

            CorrectRotation();
        }

    }

    private void RotateRight()
    {
        steinkreise[indexRows].transform.Rotate(new Vector3(0, 0, -40));
        if (indexRows != 3)
        {
            steinkreise[indexRows + 1].transform.Rotate(new Vector3(0, 0, -20));
        } else
        {
            steinkreise[0].transform.Rotate(new Vector3(0, 0, -20));
        }
        //keypressed = true;

        //Invoke("TimeAfterKey", 0.3f);
    }

    private void RotateLeft()
    {
        steinkreise[indexRows].transform.Rotate(new Vector3(0, 0, 40));
        if (indexRows != 3)
        {
            steinkreise[indexRows + 1].transform.Rotate(new Vector3(0, 0, 20));
        }
        else
        {
            steinkreise[0].transform.Rotate(new Vector3(0, 0, 20));
        }
        //keypressed = true;

        //Invoke("TimeAfterKey", 0.3f);
    }

    private void SwitchRows()
    {
        if (indexRows != 3)
        {
            indexRows++;
            steinkreise_mat[indexRows].GetComponent<MeshRenderer>().material = gameManager.materials_rows_selected[indexRows];
            steinkreise_mat[indexRows-1].GetComponent<MeshRenderer>().material = gameManager.materials_rows_start[indexRows-1];

        }
        else
        {
            indexRows = 0;
            steinkreise_mat[indexRows].GetComponent<MeshRenderer>().material = gameManager.materials_rows_selected[indexRows];
            steinkreise_mat[3].GetComponent<MeshRenderer>().material = gameManager.materials_rows_start[3];

        }
        //rowSwitched = true;

        //Invoke("TimeAfterRow", 0.5f);
    }

    /*
    private void TimeAfterKey()
    {
        keypressed = false;
    }

    private void TimeAfterRow()
    {
        rowSwitched = false;
    }
    */
    
    private void CorrectRotation()
    {
        if (UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[0].transform).z == 0 || UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[0].transform).z == 180)
        {
            gameManager.correctRow01 = true;
        }
        else
        {
            gameManager.correctRow01 = false;
        }

        if (UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[1].transform).z == 0 || UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[1].transform).z == 180)
        {
            gameManager.correctRow02 = true;
        }
        else
        {
            gameManager.correctRow02 = false;
        }

        if (UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[2].transform).z == 0 || UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[2].transform).z == 180)
        {
            gameManager.correctRow03 = true;
        }
        else
        {
            gameManager.correctRow03 = false;
        }

        if (UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[3].transform).z == 0 || UnityEditor.TransformUtils.GetInspectorRotation(steinkreise[3].transform).z == 180)
        {
            gameManager.correctRow04 = true;

        }
        else
        {
            gameManager.correctRow04 = false;
        }

        if (gameManager.correctRow01 == true && gameManager.correctRow02 == true && gameManager.correctRow03 == true && gameManager.correctRow04 == true)
        {
            gameManager.correctRows = true;
            steinkreise_mat[0].GetComponent<MeshRenderer>().material = gameManager.materials_rows_final[0];
            steinkreise_mat[1].GetComponent<MeshRenderer>().material = gameManager.materials_rows_final[1];
            steinkreise_mat[2].GetComponent<MeshRenderer>().material = gameManager.materials_rows_final[2];
            steinkreise_mat[3].GetComponent<MeshRenderer>().material = gameManager.materials_rows_final[3];
            gameManager.puzzleCounter++;
            laser01.SetActive(true);
            laser02.SetActive(true);
        }
    }
}
