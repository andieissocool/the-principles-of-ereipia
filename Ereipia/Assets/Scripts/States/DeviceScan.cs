﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class DeviceScan : State
{
	public DeviceScan(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("DeviceScan loaded");
		
		gameManager.character.GetComponent<FieldOfView>().enabled = true;
		gameManager.GUI.SetActive(true);
		gameManager.center.SetActive(true);
		gameManager.scanVolume.SetActive(true);

		gameManager.guardian1ani.SetActive(true);

		ExecuteAfterTime(1);
	}

	public override void ExitState()
	{
		Debug.Log("DeviceScan ended");

		gameManager.character.GetComponent<FieldOfView>().enabled = false;

		List<Transform> targets = gameManager.character.GetComponent<FieldOfView>().visibleTargets;
		foreach (Transform target in targets)
		{
			Debug.Log("Target from DeviceScan: " + target);
			target.GetComponent<Outline>().enabled = false;
		}

		gameManager.GUI.SetActive(false);
		gameManager.center.SetActive(false);
		gameManager.scanVolume.SetActive(false);
		gameManager.Encode();

		gameManager.guardian1ani.SetActive(false);
	}

	public override void Update()
	{
		if (Input.GetKeyDown("t") && SceneManager.GetActiveScene().name == "AudioPuzzle")
		{
			gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
		}
		else if (Input.GetKeyDown("t") && SceneManager.GetActiveScene().name == "Landmass")
		{
			gameManager.SM.ChangeState(gameManager.walkingState);
		}
		if(Input.GetKeyDown("c"))
		{
			gameManager.SM.ChangeState(gameManager.peaMenu);
		}
	}

	IEnumerator ExecuteAfterTime(int sec)
	{
		Debug.Log("execute");
		yield return new WaitForSeconds(sec);
        if (SceneManager.GetActiveScene().name == "Landmass")
        {
			gameManager.SM.ChangeState(gameManager.walkingState);
        }
        else
        {
			gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
		}
	}

}

