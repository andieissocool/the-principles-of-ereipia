﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using KinematicCharacterController;

public class PeaState : State
{
    public PeaState(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("peaState started");



		/*deactivate fpsControlelr*/
		gameManager.fpsControllerGroup.SetActive(false);

		/*deactivate bee scripts of fpsControlelr*/
		gameManager.bee1.GetComponent<Bees>().enabled = false;
		gameManager.bee2.GetComponent<Bees>().enabled = false;
		gameManager.bee3.GetComponent<Bees>().enabled = false;

		/*set cursor active*/
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;

		/*set static gui elements active*/
		gameManager.mittelkreuz.SetActive(false);
		gameManager.openMenu.SetActive(false);
		gameManager.GUI.SetActive(true);
		gameManager.guiCam.SetActive(true);
		gameManager.guiEventSystem.SetActive(true);
		gameManager.subtibtleMenu.SetActive(false);
		gameManager.newLog.SetActive(false);

		/*if(gameManager.firstTimeDone != true)
		{
			gameManager.warningMenu.SetActive(true);

			if(gameManager.firstRepairStart)
			{
				gameManager.firstTimeDone = true;
			}
		}*/

		/*set menu active*/
		gameManager.peaMainMenu.SetActive(true);
	}

	public override void ExitState()
	{
		Debug.Log("peaState ended");

		/*reactivate fpsControlelr*/
		gameManager.fpsControllerGroup.SetActive(true);

		/*reactivate bee scripts*/
		gameManager.bee1.GetComponent<Bees>().enabled = true;
		gameManager.bee2.GetComponent<Bees>().enabled = true;
		gameManager.bee3.GetComponent<Bees>().enabled = true;

		/*deactivate cursor*/
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;

		/*deactivate static gui elements*/
		gameManager.guiEventSystem.SetActive(false);
		gameManager.guiCam.SetActive(false);

		/*deactivate menu active*/
		gameManager.guiCam.SetActive(false);
		gameManager.peaMainMenu.SetActive(false);
		gameManager.msgMenu.SetActive(false);
		gameManager.runesMenu.SetActive(false);
		gameManager.guardiansMenu.SetActive(false);
		gameManager.logMenu.SetActive(false);
		gameManager.mittelkreuz.SetActive(true);
		gameManager.newLog.SetActive(false);
        gameManager.newRepairedLog.SetActive(false);

    }

	public override void Update()
	{
		if (Input.GetKeyDown("c") && SceneManager.GetActiveScene().name == "AudioPuzzle")
		{
			gameManager.SM.ChangeState(gameManager.undergroundPuzzle);
        }
		else if (Input.GetKeyDown("c") && SceneManager.GetActiveScene().name == "Landmass")
		{
			gameManager.SM.ChangeState(gameManager.walkingState);
		}
	}
}
