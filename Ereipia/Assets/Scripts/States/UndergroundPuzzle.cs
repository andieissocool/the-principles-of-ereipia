﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UndergroundPuzzle : State
{
	public UndergroundPuzzle(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

	bool check = false;

	public override void EnterState()
	{
		gameManager.subtibtleMenu.SetActive(false);
		gameManager.interactionMenu.SetActive(false);
		gameManager.mittelkreuz.SetActive(true);

		Debug.Log("Underground Scene loaded");
	}

	public override void ExitState()
	{
		Debug.Log("Pause game");
		gameManager.mittelkreuz.SetActive(false);
	}

	public override void Update()
	{
		if (gameManager.result.Length == 6 && gameManager.result == "friend")
		{
			gameManager.door.GetComponent<Animator>().SetBool("finished", true);
			gameManager.doorAudio.GetComponent<AudioSource>().Play();
			gameManager.doorAudio2.GetComponent<AudioSource>().Play();
			gameManager.result = "finished";
			gameManager.saveObject.GetComponent<SaveGame>().Save();
		}
		else if (gameManager.result.Length == 6 && gameManager.result != "friend")
		{
			gameManager.counter = 0;
			gameManager.code.Clear();
		}
        if (gameManager.result == "finished" && !check)
        {
			gameManager.code.Add("f");
			gameManager.code.Add("r");
			gameManager.code.Add("i");
			gameManager.code.Add("e");
			gameManager.code.Add("n");
			gameManager.code.Add("d");

			gameManager.Images[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("f");
			gameManager.Images[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("r");
			gameManager.Images[2].GetComponent<Image>().sprite = Resources.Load<Sprite>("i");
			gameManager.Images[3].GetComponent<Image>().sprite = Resources.Load<Sprite>("e");
			gameManager.Images[4].GetComponent<Image>().sprite = Resources.Load<Sprite>("n");
			gameManager.Images[5].GetComponent<Image>().sprite = Resources.Load<Sprite>("d");
			gameManager.door.GetComponent<Animator>().SetBool("finished", true);

			check = true;
		}

		if (gameManager.activated == 1)
		{
			gameManager.CrystalSound.GetComponent<AudioSource>().Play();
			gameManager.LightningBall.GetComponent<ParticleSystem>().Play();
			gameManager.Afterburner.GetComponent<ParticleSystem>().Play();

		}

		if (Input.GetKeyDown("p"))
		{
			gameManager.SM.ChangeState(gameManager.staticMenue);
		}


		if (Input.GetKeyDown("c"))
		{
			gameManager.SM.ChangeState(gameManager.peaMenu);
		}

		if (Input.GetKeyDown("t"))
		{
			gameManager.SM.ChangeState(gameManager.deviceScan);
		}
	}

}
