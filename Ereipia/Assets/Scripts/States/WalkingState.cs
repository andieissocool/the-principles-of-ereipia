﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WalkingState : State
{
	public WalkingState(StateMachine stateMachine, GameManager gameManager) : base(stateMachine, gameManager) { }

	public override void EnterState()
	{
		Debug.Log("Game Screen loaded - walking state started");

		gameManager.subtibtleMenu.SetActive(false);
		gameManager.interactionMenu.SetActive(false);
		gameManager.mittelkreuz.SetActive(true);

		/*gameManager.fpsControllerGroup.SetActive(false);
		gameManager.bee1.GetComponent<Bees>().enabled = false;
		gameManager.bee2.GetComponent<Bees>().enabled = false;
		gameManager.bee3.GetComponent<Bees>().enabled = false;*/

		if(gameManager.firstTimeDone)
		{
			AudioSource audio = gameManager.GetComponent<AudioSource>();
			audio.PlayOneShot(gameManager.peaDamaged, gameManager.Volume);
			gameManager.peaDamagedPlayed = true;
		}
	}

	public override void ExitState()
	{
		Debug.Log("Pause game");
		gameManager.mittelkreuz.SetActive(false);
	}

	public override void Update()
	{
		if (Input.GetKeyDown("p"))
		{
			gameManager.SM.ChangeState(gameManager.staticMenue);
		}

		if (Input.GetKeyDown("c"))
		{
			gameManager.SM.ChangeState(gameManager.peaMenu);
		}

		if (Input.GetKeyDown("t"))
		{
			gameManager.SM.ChangeState(gameManager.deviceScan);
		}

        if (gameManager.steinkreis_active)
        {
            gameManager.SM.ChangeState(gameManager.roatingPuzzle);
        }
        if (gameManager.character.GetComponent<keyspressed>().triggerTemple == true)
        {
            gameManager.SM.ChangeState(gameManager.finalPuzzle);
        }
    }

    }
