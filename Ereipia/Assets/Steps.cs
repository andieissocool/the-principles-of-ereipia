﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinematicCharacterController;
using GAD;

public class Steps : MonoBehaviour
{
    public AudioClip jump;
    public AudioClip land;
    public AudioClip[] list;
    public AudioClip jumpGrass;
    public AudioClip landGrass;
    public AudioClip[] listGrass;
    public GameObject character;

    void OnTriggerEnter(Collider player)
    {
        if (player.tag == "Character")
        {
            character.GetComponent<GADCharacterController>().JumpSound = jump;
            character.GetComponent<GADCharacterController>().LandSound = land;
            character.GetComponent<GADCharacterController>().FootstepSounds = list;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Character")
        {
            character.GetComponent<GADCharacterController>().JumpSound = jumpGrass;
            character.GetComponent<GADCharacterController>().LandSound = landGrass;
            character.GetComponent<GADCharacterController>().FootstepSounds = listGrass;
        }
    }
}
