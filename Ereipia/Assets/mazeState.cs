﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mazeState : MonoBehaviour
{
    public GameManager gameManager;
    void OnTriggerEnter() {
        gameManager.SM.ChangeState(gameManager.labyrinthPuzzle);
        Destroy(this);
    }
}
