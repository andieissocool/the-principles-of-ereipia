﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinematicCharacterController;
using GAD;

[RequireComponent(typeof(PhysicsMover))]
public class StickyWall : MonoBehaviour, IMoverController
{
    public Vector3 gravity = new Vector3(0, -30, 0);

    Vector3 initPos;
    Quaternion initRotation;

    void Awake()
    {
        GetComponent<PhysicsMover>().MoverController = this; // important! add this to the mover, or you'll get an exceptopn
        initPos = transform.position;
        initRotation = transform.rotation;
    }

    public void UpdateMovement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
    {
        goalPosition = initPos;
        goalRotation = initRotation;
    }

    private void OnDrawGizmos()
    {
        const float arrowSize = 0.3f;

        Vector3 lineStart = transform.position;
        Vector3 lineEnd = lineStart + gravity;
        Vector3 side;

        if (Vector3.Cross(gravity, Vector3.up) != Vector3.zero)
        {
            side = Vector3.Cross(gravity, Vector3.up).normalized * arrowSize;
        }
        else
        {
            side = Vector3.Cross(gravity, Vector3.right).normalized * arrowSize;
        }

        Gizmos.DrawLine(lineStart, lineEnd);

        Gizmos.DrawLine(lineEnd, lineEnd + side + (lineStart - lineEnd).normalized * arrowSize);
        Gizmos.DrawLine(lineEnd, lineEnd - side + (lineStart - lineEnd).normalized * arrowSize);
    }
}
