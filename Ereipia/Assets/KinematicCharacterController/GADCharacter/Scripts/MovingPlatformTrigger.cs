﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformTrigger : MonoBehaviour
{
    public enum TriggerAction { start, stop };

    public MovingPlatform platformToTrigger;
    public TriggerAction action = TriggerAction.start;
    public string playerTag = "Player";

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == playerTag)
        {
            if (action == TriggerAction.start)
                platformToTrigger.StartMoving();
            else
                platformToTrigger.StopMoving();
        }
    }
}
