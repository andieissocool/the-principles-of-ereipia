﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinematicCharacterController;

[RequireComponent(typeof(PhysicsMover))]
public class MovingPlatform : MonoBehaviour, IMoverController
{
    public Transform from;
    public Transform to;
    public float duration = 5f;
    public AnimationCurve easing = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public bool autostart = true;

    private Transform _transform;
    private bool _isMoving;
    private float _curPeriod;
    private Vector3 _nextPosition;
    private Quaternion _nextRotation;

    // Method from IMoveController to make moving platforms.
    // You don't actually move the platform, but just pass the position it's supposed to be via this method
    public void UpdateMovement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
    {
        if (_isMoving)
        {
            _curPeriod = Mathf.Repeat(_curPeriod + deltaTime / duration, 1f);
            float d;

            if (_curPeriod < 0.5) // from > to
            {
                d = easing.Evaluate(_curPeriod * 2);
            }
            else // to > from
            {
                d = easing.Evaluate(2 - _curPeriod * 2);
            }

            _nextPosition = Vector3.Lerp(from.position, to.position, d);
            _nextRotation = Quaternion.Lerp(from.rotation, to.rotation, d);
        }

        goalPosition = _nextPosition;
        goalRotation = _nextRotation;
    }

    void Start()
    {
        _transform = transform;
        GetComponent<PhysicsMover>().MoverController = this; // important! add this to the mover, or you'll get an exceptopn
        _isMoving = autostart;
    }

    public void StartMoving()
    {
        _isMoving = true;
    }

    public void StopMoving()
    {
        _isMoving = false;
    }

    public void ResetPosition()
    {
        _curPeriod = 0;

        _nextPosition = from.position;
        _nextRotation = from.rotation;
    }
}
